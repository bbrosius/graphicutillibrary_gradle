package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

/**Draws a circle that slopes down on the sides. 
 * Y will be the height direction xz will be the circular directions
 *
 */
public class Dome implements Geometry
{
	private float height;
	private float width;
	private float yResolution = .3f;
	private float xzResolution = 10f;
	private int vertexCount;
	private int indexCount;
	private ShortBuffer indices;
	private FloatBuffer vertices;
	private static double DEG = Math.PI/180;

	public Dome(float height, float width)
	{
		this.height = height;
		this.width = width;
	}
	
	public Dome(float height, float width, float yRes, float xzRes)
	{
		this.height = height;
		this.width = width;
		this.yResolution = yRes;
		this.xzResolution = xzRes;
	}
	
	private void buildDome()
	{
		//Convert resolution to radians
		xzResolution = (float) (xzResolution * DEG);
		
		//Scale factor this will scale the concentric circles down evenly until it reaches a single point
		float scaleFactor = width / yResolution;
		
		ArrayList<Vertex> vertexList = new ArrayList<Vertex>();
		ArrayList<Short> indexList = new ArrayList<Short>();
		short levelCount = (short) ( (Math.PI * 2) / xzResolution  + 1 );
		
		short index = 0;
		for(float y = 0.0f; y < height; y += yResolution)
		{
			for(float j = 0.0f; j < Math.PI * 2; j += xzResolution)
			{
				float radius = width;
				float x = (float) (radius * Math.cos(j));
				float z = (float) (radius * Math.sin(j));
				vertexList.add(new Vertex(x, y, z));

				//Don't add indices for the top row
				if( y + yResolution < height )
				{
					//Create just one triangle for the first index
					if(j == 0)
					{
						indexList.add(index);
						indexList.add((short)(index + 1));
						indexList.add((short)(index + levelCount));
					}
					else if(j == levelCount - 1) //Create the single end triangle
					{
						indexList.add(index);
						short up = (short) (index + levelCount);
						indexList.add(up);
						indexList.add((short)(up - 1));
					}
					else
					{
						indexList.add(index);
						short up = (short) (index + levelCount);
						indexList.add(up);
						indexList.add((short)(up - 1));
						
						indexList.add(index);
						indexList.add((short)(index + 1));
						indexList.add((short)(index + levelCount));
					}
				}
			}
		}
		
		//Add the center point
		vertexList.add(new Vertex(0, height, 0));
	}
	
	@Override
	public FloatBuffer getVertexBuffer() 
	{
		return vertices;
	}

	@Override
	public ShortBuffer getIndexBuffer() 
	{
		return indices;
	}

	@Override
	public FloatBuffer getNormalBuffer() 
	{
		return null;
	}

	@Override
	public int getVertexCount() 
	{
		return vertexCount;
	}

	@Override
	public int getIndexCount() 
	{
		return indexCount;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void releaseVertexBuffer() 
	{
		if( vertices != null )
		{
			vertices.clear();
			vertices = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indices != null )
		{
			indices.clear();
			indices = null;
		}
	}
	
}