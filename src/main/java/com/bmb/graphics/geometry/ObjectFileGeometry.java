package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import com.bmb.graphics.utils.GraphicUtils;

//Defines a generic 3d object
public class ObjectFileGeometry implements Geometry
{
	private FloatBuffer vertices;
	private ShortBuffer indices;
	private FloatBuffer normals;
	
	private int vertCount;
	private int indexCount;
	
	private float minX;
	private float maxX;
	
	private float minY;
	private float maxY;
	
	private float minZ;
	private float maxZ;
	
	public ObjectFileGeometry(ArrayList<Float> vertices, ArrayList<Float> normals, ArrayList<Short> indices)
	{
		vertCount = vertices.size();
		indexCount = indices.size();
		
		this.vertices = GraphicUtils.convertFloatArrayListToFloatBuffer(vertices);
		this.normals = GraphicUtils.convertFloatArrayListToFloatBuffer(normals);
		this.indices = GraphicUtils.convertShortArrayListToShortBuffer(indices);
	}
	
	@Override
	public FloatBuffer getVertexBuffer()
	{
		return vertices;
	}
	
	@Override
	public FloatBuffer getNormalBuffer()
	{
		return normals;
	}
	
	@Override
	public ShortBuffer getIndexBuffer()
	{
		return indices;
	}
	
	@Override
	public int getVertexCount()
	{
		return vertCount;
	}
	
	@Override
	public int getIndexCount()
	{
		return indexCount;
	}
	
	public void draw(int DrawingType)
	{
		
	}
	
	public float getMinX()
	{
		return minX;
	}
	
	public void setMinX(float x)
	{
		minX = x;
	}
	
	public float getMaxX()
	{
		return maxX;
	}
	
	public void setMaxX(float x)
	{
		maxX = x;
	}
	
	public void setMinMaxX(float min, float max)
	{
		minX = min;
		maxX = max;
	}
	
	public float getMinY()
	{
		return minY;
	}
	
	public void setMinY(float y)
	{
		minY = y;
	}
	
	public float getMaxY()
	{
		return maxY;
	}
	
	public void setMaxY(float y)
	{
		maxY = y;
	}
	
	public void setMinMaxY(float min, float max)
	{
		minY = min;
		maxY = max;
	}
	
	public float getMinZ()
	{
		return minZ;
	}
	
	public void setMinZ(float z)
	{
		minZ = z;
	}

	public float getMaxZ()
	{
		return maxZ;
	}
	
	public void setMaxZ(float z)
	{
		maxZ = z;
	}
	
	public void setMinMaxZ(float min, float max)
	{
		minZ = min;
		maxZ = max;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void releaseVertexBuffer()
	{
		if( vertices != null )
		{
			vertices.clear();
			vertices = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indices != null )
		{
			indices.clear();
			indices = null;
		}
	}
}