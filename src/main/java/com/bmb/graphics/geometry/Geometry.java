package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public interface Geometry
{
	/**
	 * This will be used to distinguish what type of texture is being drawn. 
	 * The texture coords for image will be flipped along the y axis to match the image standard of 0,0 being top left. 
	 * Render type is used for rendering a scene to a texture this is used to match 0,0 to the bottom right. 
	 *
	 */
	public static enum TextureType {
		IMAGE,
		RENDER,
	};
	

	/**
	 * Returns the vertex buffer for the geometry. 
	 * @return The float buffer representing the vertices, or null if it has already been released.
	 */
	public FloatBuffer getVertexBuffer();
	
	/**
	 * Releases the vertex buffer resources after it has been passed to the gpu. 
	 * The buffer will be null when complete.
	 */
	public void releaseVertexBuffer();
	
	/**
	 * Returns the short buffer representing the indicies of the geometry for drawing. 
	 * @return The short buffer representing the indicies of the geometry, or null if it has been released already.
	 */
	public ShortBuffer getIndexBuffer();
	
	/**
	 * Releases the resources associated with the index buffer after it has been passed to the gpu.
	 * The buffer will be null when complete. 
	 */
	public void releaseIndexBuffer();
	
	public FloatBuffer getNormalBuffer();
	
	public FloatBuffer getTextureCoordBuffer(TextureType type);
	
	public int getVertexCount();
	
	public int getIndexCount();
	
	public int getTextureCoordCount(TextureType type);
}