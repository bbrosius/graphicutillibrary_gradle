package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import com.bmb.graphics.utils.GraphicUtils;

public class Cube implements Geometry
{
	private Polygon front;
	private Polygon right;
	private Polygon back;
	private Polygon left;
	private Polygon top;
	private Polygon bottom;
	private Vertex[] vertices;
	
	private byte indices[] = {
    		0, 1, 3, 0, 3, 2, 				
    };
	
	private FloatBuffer buffer;
	
	private Cube(Vertex bottomLeftCorner, Vertex topRightCorner)
	{
		vertices = new Vertex[24];
		
		float nearZ = bottomLeftCorner.z;
		float farZ = topRightCorner.z;
		
		float leftX = bottomLeftCorner.x;
		float rightX = topRightCorner.x;
		
		float topY = topRightCorner.y;
		float bottomY = bottomLeftCorner.y;
		
		front = new Polygon(new Vertex(leftX, bottomY, farZ), new Vertex(leftX, topY, farZ), 
						new Vertex(rightX, topY, farZ), new Vertex(rightX, bottomY, farZ));
		addVertices(0, front.getVertices());
		
		right = new Polygon(new Vertex(rightX, bottomY, farZ), new Vertex(rightX, topY, farZ),
						new Vertex(rightX, topY, nearZ), new Vertex(rightX, bottomY, nearZ));
		addVertices(4, right.getVertices());
		
		back = new Polygon(new Vertex(leftX, bottomY, nearZ), new Vertex(rightX, bottomY, nearZ),
						new Vertex(rightX, topY, nearZ), new Vertex(leftX, topY, nearZ));
		addVertices(8, back.getVertices());
		
		left = new Polygon(new Vertex(leftX, bottomY, farZ), new Vertex(leftX, topY, farZ),
						new Vertex(leftX, topY, nearZ), new Vertex(leftX, bottomY, nearZ));
		addVertices(12, left.getVertices());
		
		top = new Polygon(new Vertex(leftX, topY, nearZ), new Vertex(rightX, topY, nearZ),
						new Vertex(rightX, topY, farZ), new Vertex(leftX, topY, farZ));
		addVertices(16, top.getVertices());
		
		bottom = new Polygon(new Vertex(leftX, bottomY, nearZ), new Vertex(leftX, bottomY, farZ), 
						new Vertex(rightX, bottomY, farZ), new Vertex(rightX, bottomY, nearZ));
		addVertices(20, bottom.getVertices());
		
		createFloatBuffer();
	}
	
	/**
	 * Vertices should be entered counterclockwise starting from bottom left with the bottom of the cube. Followed by
	 * the top counterclockwise from the bottom left.
	 */
	private Cube(Vertex bottomLeftNear, Vertex bottomRightNear, Vertex bottomRightFar, Vertex bottomLeftFar, Vertex topLeftNear, 
			Vertex topRightNear, Vertex topRightFar, Vertex topLeftFar)
	{
		vertices = new Vertex[24];

		front = new Polygon(bottomLeftFar, topLeftFar, topRightFar, bottomRightFar);
		addVertices(0, front.getVertices());
		
		right = new Polygon(bottomRightFar, topRightFar, topRightNear, bottomRightNear);
		addVertices(4, right.getVertices());

		back = new Polygon(bottomLeftNear, bottomRightNear, topRightNear, topLeftNear);
		addVertices(8, back.getVertices());

		left = new Polygon(bottomLeftFar, topLeftFar, topLeftNear, bottomLeftNear);
		addVertices(12, left.getVertices());

		top = new Polygon(topLeftNear, topRightNear, topRightFar, topLeftFar);
		addVertices(16, top.getVertices());

		bottom = new Polygon(bottomLeftNear, bottomLeftFar, bottomRightFar, bottomRightNear);
		addVertices(20, bottom.getVertices());
		
		createFloatBuffer();
	}
	
	public Polygon getFront()
	{
		return front;
	}
	
	public Polygon getBack()
	{
		return back;
	}
	
	public Polygon getLeft()
	{
		return left;
	}
	
	public Polygon getRight()
	{
		return right;
	}
	
	public Polygon getTop()
	{
		return top;
	}
	
	public Polygon getBottom()
	{
		return bottom;
	}

	private void addVertices(int index, Vertex... verts)
	{
		for( int i = 0; i < verts.length; i++)
		{
			vertices[index + i] = verts[i];
		}
	}
	
	private void createFloatBuffer()
	{
		float[] vertCoords = GraphicUtils.convertVertexArrayToFloatArray(vertices);
		buffer = GraphicUtils.convertFloatArrayToFloatBuffer(vertCoords);
	}

	@Override
	public FloatBuffer getVertexBuffer()
	{
		return buffer;
	}

	@Override
	public ShortBuffer getIndexBuffer()
	{
		return null;
	}

	@Override
	public FloatBuffer getNormalBuffer() 
	{
		return null;
	}

	@Override
	public int getVertexCount() 
	{
		return vertices.length;
	}

	@Override
	public int getIndexCount() 
	{
		return 0;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) 
	{
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type) 
	{
		return 0;
	}

	@Override
	public void releaseVertexBuffer() 
	{
		if( buffer != null )
		{
			buffer.clear();
			buffer = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{		
	}
}