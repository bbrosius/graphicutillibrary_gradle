package com.bmb.graphics.geometry;

import android.graphics.Matrix;
import com.bmb.graphics.utils.GraphicUtils;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

/**
 * Class to represent Quad objects since they are not supported directly by gles. 
 * 
 *
 */
public class Polygon implements Geometry
{
	private Vertex[] vertices;
	private FloatBuffer buffer;
	private ShortBuffer indexBuffer;
	private FloatBuffer imageTextureCoordBuffer;
	private FloatBuffer renderTextureCoordBuffer;
	
	private Vertex center;

    private float minX;
    private float maxX;

    private float minY;
    private float maxY;

    private float width;
    private float height;
	
	private short indices[] = {
    		0, 1, 2, 0, 2, 3, 				
    };
	
	private float imageTextureCoords[] = {
			0, 1, 
			1, 1,
			1, 0, 
			0, 0,
			0, 1,
			1, 1			
	};
	
	private float renderTextureCoords[] = {
			0, 0, 
			1, 0,
			1, 1,
			0, 1,
			0, 0, 
			1, 0
	};

	/**
	 * Vertices should be defined counter clockwise from bottom left to top right
	 * @param vertices The vertices that make up the polygon
	 */
	public Polygon(Vertex... vertices)
	{
		this.vertices = vertices;
		
		findBounds();
		
		createBuffers();
	}

    /**
     * Creates a square around the center point
     * @param center The center of the square
     * @param width The width of the square
     * @param height The height of the square
     */
	public Polygon(Vertex center, float width, float height)
	{
        vertices = new Vertex[4];

        this.width = width;
        this.height = height;

		minX = center.x - (width/2.0f);
		maxX = center.x + (width/2.0f);
		
		maxY = center.y + (height/2.0f);
        minY = center.y - (height/2.0f);

        this.center = center;

		vertices[0] = new Vertex(minX, minY, center.z);
        vertices[1] = new Vertex(maxX, minY, center.z);
        vertices[2] = new Vertex(maxX, maxY, center.z);
        vertices[3] = new Vertex(minX, maxY, center.z);

		createBuffers();
	}

    public Polygon(float minX, float maxX, float minY, float maxY)
    {
        vertices = new Vertex[4];

        this.minX = minX;
        this.minY = minY;

        this.maxX = maxX;
        this.maxY = maxY;

        this.width = maxX - minX;
        this.height = maxY - minY;

        center = new Vertex((maxX + minX) / 2, (maxY + minY) / 2, 0.0f);

        vertices[0] = new Vertex(minX, minY, center.z);
        vertices[1] = new Vertex(maxX, minY, center.z);
        vertices[2] = new Vertex(maxX, maxY, center.z);
        vertices[3] = new Vertex(minX, maxY, center.z);

        createBuffers();
    }

    public Vertex getVertex(int index)
	{
		Vertex vertex = null;
		if( index < vertices.length )
		{
			vertex = vertices[index];
		}
		return vertex;
	}
	
	public Vertex[] getVertices()
	{
		return vertices;
	}
	
	private void createBuffers()
	{
		float[] vertArray = GraphicUtils.convertVertexArrayToFloatArray(vertices);
        buffer = GraphicUtils.convertFloatArrayToFloatBuffer(vertArray);
        
        indexBuffer = GraphicUtils.convertShortArrayToShortBuffer(indices);
        imageTextureCoordBuffer = GraphicUtils.convertFloatArrayToFloatBuffer(imageTextureCoords);
        renderTextureCoordBuffer = GraphicUtils.convertFloatArrayToFloatBuffer(renderTextureCoords);
	}

    /**
     * Gets the width of the polygon at the widest part.
     *
     * @return The max width of the polygon
     */
    public float getWidth()
    {
        return width;
    }

    /**
     * Gets the height of hte polygon at the tallest part
     * @return The max height of the polygon
     */
    public float getHeight()
    {
        return height;
    }

    /**
     * Gets the smallest x value of the polygon.
     * @return The smallest x value of the polygon
     */
    public float getMinX()
    {
        return minX;
    }

    /**
     * Gets the maximum x value of the polygon
     * @return The largest x value of the polygon
     */
    public float getMaxX()
    {
        return maxX;
    }

    /**
     * Gets the minimum y value of the polygon
     * @return The smallest y value of the polygon
     */
    public float getMinY()
    {
        return minY;
    }

    /**
     * Gets the maximum y value of the polygon
     * @return The largest y value of the polygon
     */
    public float getMaxY()
    {
        return maxY;
    }

    /**
     * Returns the center point of the polygon
     * @return Find the center of of the polygon, in this case simply the mid x, y point
     */
    public Vertex getCenter()
    {
        return center;
    }

    /**
     * Checks if the passed in vertex is within the bounds of the polygon.
     *
     * @param vertex The vertex to check if it lies in bounds of the polygon
     * @return true if the vertex is inside the bounds of the polygon, false otherwise
     */
    public boolean contains(Vertex vertex)
    {
        int i;
        int j;
        boolean result = false;
        for( i = 0, j = vertices.length - 1; i < vertices.length; j = i++ )
        {
            if( (vertices[i].y > vertex.y) != (vertices[j].y > vertex.y) &&
                    (vertex.x < (vertices[j].x - vertices[i].x) * (vertex.y - vertices[i].y) / (vertices[j].y - vertices[i].y) + vertices[i].x) )
            {
                result = !result;
            }
        }
        return result;
    }

    /**
     * Performs a simplifed contains checking that checks if the point is in the rectangular max bounds of the polygon.
     * This can be used for fast, inaccurate calculations. Note this function has high accuracy for rectangle polygons.
     *
     * @param x The x value to check if it lies in the polygon
     * @param y The y value to check if it lies in the polygon
     * @return True if the point lies with in the rectangular bounds of the polgyong.
     */
    public boolean simpleContains(float x, float y)
    {
        return minX <= x && x <= maxX && minY <= y && y <= maxY;
    }

    /**
     * Performs a simplifed contains checking that checks if the point is in the rectangular max bounds of the polygon.
     * This can be used for fast, inaccurate calculations. Note this function has high accuracy for rectangle polygons.
     *
     * @param vertex The vertex to check if it lies in bounds of the polygon
     * @return True if the point lies with in the rectangular bounds of the polgyong.
     */
    public boolean simpleContains(Vertex vertex)
    {
        return simpleContains(vertex.x, vertex.y);
    }

    /**
     * Checks if the rectangular bounds intersects with the rectangular bounds of the polygon.
     *
     * @param minX The minimum x bounds of the intersecting rectangle
     * @param maxX The maximum x bounds of the intersecting rectangle
     * @param minY The minimum y bounds of the intersecting rectangle
     * @param maxY The maximum y bounds of the intersecting rectangle
     * @return True if the bounds intersect, false otherwise.
     */
    public boolean intersects(double minX, double maxX, double minY, double maxY)
    {
        return minX < this.maxX && this.minX < maxX && minY < this.maxY && this.minY < maxY;
    }

    /**
     * Checks if the rectangular max bounds of the given polygon intersect with the max rectangular bounds of the polygon.
     *
     * @param bounds The bounds of the intersectiong
     * @return True if the bounds intersect, false otherwise.
     */
    public boolean intersects(Polygon bounds)
    {
        return intersects(bounds.minX, bounds.maxX, bounds.minY, bounds.maxY);
    }

    /**
     * Checks if the max rectangular bounds of the passed in polygon is contained by the max rectangular bounds of the polygon.
     *
     * @param bounds The bounds of the polygon to check
     * @return True if the provided polygon is contained by this polygon, false otherwise.
     */
    public boolean contains(Polygon bounds)
    {
        return bounds.minX >= minX && bounds.maxX <= maxX && bounds.minY >= minY && bounds.maxY <= maxY;
    }

	private void findBounds()
	{
		minY = Float.MAX_VALUE;
		minX = Float.MAX_VALUE;
		float minZ = Float.MAX_VALUE;
		
		maxY = Float.MIN_VALUE;
		maxX = Float.MIN_VALUE;
		float maxZ = Float.MIN_VALUE;		
		
		for(Vertex vertex : vertices)
		{
			if(vertex.x < minX)
			{
				minX = vertex.x;
			}
			
			if(vertex.x > maxX)
			{
				maxX = vertex.x;
			}
			
			if(vertex.y < minY)
			{
				minY = vertex.y;
			}
			
			if(vertex.y > maxY)
			{
				maxY = vertex.y;
			}
			
			if(vertex.z < minZ)
			{
				minZ = vertex.z;
			}
			
			if(vertex.z > maxZ)
			{
				maxZ = vertex.z;
			}
		}

        width = maxX - minX;
        height = maxY - minY;

        center = new Vertex((maxX + minX)/2, (maxY + minY)/2, (maxZ + maxZ)/2);
    }
	
	public void rotate(float theta, char axis)
	{
		Vertex newVert = new Vertex(0,0,0);
		
		for( int i = 0; i < vertices.length; i++)
		{
			if( axis == 'z')
			{
				rotate(vertices[i].x, vertices[i].y, theta, newVert);
				vertices[i].x = newVert.x;
				vertices[i].y = newVert.y;
			}
		}
	
		createBuffers();
	}
	
	private void rotate(float x, float y, float theta, Vertex newVert )
	{
		float [] pts = {x,y};
		
		//Convert to degrees
		theta = theta * 180/3.14159f;
		
		Matrix m = new Matrix();
		//m.postTranslate(-centerX, -centerY);
		m.setRotate(theta, center.x, center.y);
		//m.postTranslate(centerX, centerY);
		m.mapPoints(pts);
		
		newVert.x = pts[0];
		newVert.y = pts[1];
	}

	@Override
	public FloatBuffer getVertexBuffer() 
	{
		return buffer;
	}

	@Override
	public ShortBuffer getIndexBuffer() 
	{
		return indexBuffer;
	}
	
	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) 
	{
		FloatBuffer buffer;
		if( type == TextureType.IMAGE)
		{
			buffer = imageTextureCoordBuffer;
		} 
		else 
		{
			buffer = renderTextureCoordBuffer;
		}
		
		return buffer;
	}

	@Override
	public FloatBuffer getNormalBuffer() 
	{
		return null;
	}

	@Override
	public int getVertexCount() 
	{
		return vertices.length;
	}

	@Override
	public int getIndexCount() 
	{
		return indices.length;
	}
	
	@Override
	public int getTextureCoordCount(TextureType type)
	{
		int length;
		if( type == TextureType.IMAGE )
		{
			length = imageTextureCoords.length;
		}
		else
		{
			length = renderTextureCoords.length;
		}
		return length;
	}

	@Override
	public void releaseVertexBuffer() 
	{
		if( buffer != null )
		{
			buffer.clear();
			buffer = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indexBuffer != null )
		{
			indexBuffer.clear();
			indexBuffer = null;
		}
	}

    @Override
    public String toString()
    {
        return "Polygon{ Center = " + center + ", width = " + width + ", height = " + height + ", minX = " + minX + ", maxX = " +
                ", minY = " + minY + " maxY = " + maxY + "}";
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + center.hashCode();
        hash = hash * 31 + Float.floatToIntBits(width);
        hash = hash * 31 + Float.floatToIntBits(height);
        hash = hash * 31 + Float.floatToIntBits(minX);
        hash = hash * 31 + Float.floatToIntBits(maxX);
        hash = hash * 31 + Float.floatToIntBits(minY);
        hash = hash * 31 + Float.floatToIntBits(maxY);
        hash = hash * 31 + Arrays.hashCode(vertices);

        return hash;
    }

    @Override
    public boolean equals(Object other)
    {
        boolean equal = false;
        if( other instanceof Polygon )
        {
            Polygon otherPolygon = (Polygon) other;
            if( center.equals(otherPolygon.center) )
            {
                if( width == otherPolygon.width && height == otherPolygon.height )
                {
                    if( minX == otherPolygon.minX && maxX == otherPolygon.maxX
                            && minY == otherPolygon.minY && maxY == otherPolygon.maxY )
                    {
                        if( Arrays.asList(vertices).equals(Arrays.asList(otherPolygon.vertices)) )
                        {
                            equal = true;
                        }
                    }
                }
            }
        }

        return equal;
    }
}