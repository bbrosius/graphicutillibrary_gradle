package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import com.bmb.graphics.utils.GraphicUtils;

public class Ray3D implements Geometry
{
	FloatBuffer vertices;
	ShortBuffer indices;
	float yResolution =  .25f;
	float xzResolution = 36f;
	private static double DEG = Math.PI/180;

	private int vertexCount = 0;
	private int indexCount = 0;
	
	float radius = .1f;
	
	public Ray3D(float length, float yResolution)
	{
		this.yResolution = yResolution;
		generateRay(length);
	}
	
	private void generateRay(float length)
	{
		//Convert resolution to radians
		xzResolution = (float) (xzResolution * DEG);
		float slices = length / yResolution;

		//length = 2;
		ArrayList<Vertex> vertexList = new ArrayList<Vertex>();
		ArrayList<Short> indexList = new ArrayList<Short>();
		short levelCount = (short) ( (Math.PI * 2) / xzResolution  + 1 );
		short index = 0;
		for(float y = 0.0f; y <= slices; y++)
		{
			for(float j = 0.0f; j < Math.PI * 2; j += xzResolution)
			{				
				float x = (float) (radius * Math.cos(j));
				float z = (float) (radius * Math.sin(j));

				vertexList.add(new Vertex(x, y * yResolution, z));
				
				//Don't add indices for the top row
				if( y < slices )
				{
					//Create just one triangle for the first index
					if(j == 0)
					{
						indexList.add(index);
						indexList.add((short)(index + 1));
						indexList.add((short)(index + levelCount));
						
						indexList.add(index);
						indexList.add((short)(index + levelCount));
						indexList.add((short)(index + levelCount - 1));
						
					}
					else if(j == levelCount - 1) //Create the single end triangle
					{
						indexList.add(index);
						short up = (short) (index + levelCount);
						indexList.add(up);
						indexList.add((short)(up - 1));
						
						indexList.add(index);
						indexList.add((short) (index + 1 - levelCount));
						indexList.add((short) (index + levelCount));
					}
					else
					{
						indexList.add(index);
						short up = (short) (index + levelCount);
						indexList.add(up);
						indexList.add((short)(up - 1));
						
						indexList.add(index);
						indexList.add((short)(index + 1));
						indexList.add((short)(index + levelCount));
					}
				}
				index++;
			}
		}
		
		indexCount = indexList.size();
		vertexCount = vertexList.size();
		vertices = GraphicUtils.convertVertexArrayListToFloatBuffer(vertexList);
		vertexList.clear();
		indices = GraphicUtils.convertShortArrayListToShortBuffer(indexList);
		indexList.clear();
	}
	
	@Override
	public FloatBuffer getVertexBuffer()
	{
		return vertices;
	}
	
	public void setXZResolution(float res)
	{
		xzResolution = res;
	}
	
	public void setYResolution(float res)
	{
		yResolution = res;
	}
	
	public void setRadius(float radius)
	{
		this.radius = radius;
	}

	@Override
	public ShortBuffer getIndexBuffer()
    {
		return indices;
	}

	@Override
	public FloatBuffer getNormalBuffer()
    {
		return null;
	}

	@Override
	public int getVertexCount()
    {
		return vertexCount;
	}

	@Override
	public int getIndexCount() 
	{
		return indexCount;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type)
    {
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type)
    {
		return 0;
	}

	@Override
	public void releaseVertexBuffer() 
	{
		if( vertices != null )
		{
			vertices.clear();
			vertices = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indices != null )
		{
			indices.clear();
			indices = null;
		}
	}
}