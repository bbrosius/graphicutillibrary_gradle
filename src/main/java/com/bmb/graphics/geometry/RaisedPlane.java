package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import com.bmb.graphics.utils.GraphicUtils;

public class RaisedPlane implements Geometry
{
    private Vertex[] vertices;
    private FloatBuffer buffer;
    private ShortBuffer indexBuffer;
    private FloatBuffer imageTextureCoordBuffer;
    private FloatBuffer renderTextureCoordBuffer;

    private float centerX;
    private float centerY;
    private float centerZ;

    private short indices[] = {
            0, 1, 4, 1, 2, 4, 2, 3, 4, 3, 0, 4
    };

    private float imageTextureCoords[] = {
            0, 1,
            1, 1,
            1, 0,
            0, 0,
            0, 1,
            1, 1
    };

    private float renderTextureCoords[] = {
            0, 0,
            1, 0,
            1, 1,
            0, 1,
            0, 0,
            1, 0
    };

    public RaisedPlane()
    {

    }

    /**
     * Vertices should be defined counter clockwise from bottom left to top right
     * @param vertices
     */
    public RaisedPlane(Vertex center, Vertex... vertices)
    {
        this.vertices = new Vertex[5];

        for( int i = 0; i < 4; i++ )
        {
            this.vertices[i] = vertices[i];
        }

        this.vertices[4] = center;

        createBuffers();
    }

    public RaisedPlane(Vertex center, float width, float height, float depth)
    {
        vertices = new Vertex[4];
        float left = center.x - (width/2);
        float right = center.x + (width/2);

        //float top =
        //Vertex lowerLeft = ()
        createBuffers();
    }

    private void createBuffers()
    {
        float[] vertArray = GraphicUtils.convertVertexArrayToFloatArray(vertices);
        buffer = GraphicUtils.convertFloatArrayToFloatBuffer(vertArray);

        indexBuffer = GraphicUtils.convertShortArrayToShortBuffer(indices);
        imageTextureCoordBuffer = GraphicUtils.convertFloatArrayToFloatBuffer(imageTextureCoords);
        renderTextureCoordBuffer = GraphicUtils.convertFloatArrayToFloatBuffer(renderTextureCoords);
    }

    @Override
    public FloatBuffer getVertexBuffer() 
    {
        return buffer;
    }

    @Override
    public ShortBuffer getIndexBuffer() 
    {
        return indexBuffer;
    }

    @Override
    public FloatBuffer getTextureCoordBuffer(TextureType type) 
    {
        FloatBuffer buffer = null;
        if( type == TextureType.IMAGE)
        {
            buffer = imageTextureCoordBuffer;
        }
        else
        {
            buffer = renderTextureCoordBuffer;
        }

        return buffer;
    }

    @Override
    public FloatBuffer getNormalBuffer() 
    {
        return null;
    }

    @Override
    public int getVertexCount() 
    {
        return vertices.length;
    }

    @Override
    public int getIndexCount() 
    {
        return indices.length;
    }

    @Override
    public int getTextureCoordCount(TextureType type) 
    {
        int length = 0;
        if( type == TextureType.IMAGE )
        {
            length = imageTextureCoords.length;
        }
        else
        {
            length = renderTextureCoords.length;
        }
        return length;
    }

	@Override
	public void releaseVertexBuffer() 
	{
		if( buffer != null )
		{
			buffer.clear();
			buffer = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indexBuffer != null )
		{
			indexBuffer.clear();
			indexBuffer = null;
		}
	}
}
