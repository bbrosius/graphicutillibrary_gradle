package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import com.bmb.graphics.utils.GraphicUtils;

public class Sphere implements Geometry
{
	
	private FloatBuffer vertexBuffer;
	private ShortBuffer indexBuffer;
	
	private static double DEG = Math.PI/180;
	private double radius;
	private Vertex center;
	
	private int vertexCount;
	private int indexCount;
	
	public Sphere(double r, int subdivision, Vertex center)
	{
        radius = r;
        this.center = center;
        vertexCount = 0;       
        
        createSphere(subdivision);
	}
	
	private void createSphere(int cuts)
	{
		double deltaTheta = (Math.PI * 2)/cuts;
		double deltaPhi = Math.PI/cuts;
		
		ArrayList<Vertex> vertices = new ArrayList<Vertex>();
		ArrayList<Short> indexList = new ArrayList<Short>();

		short levelCount = (short) ( (Math.PI * 2) / deltaTheta  + 1 );
		short lastIndex = (short) ( ( ( cuts - 1 ) * levelCount ) + 1);
		short index = 0;

		for(double phi = 0; phi <= Math.PI; phi+=deltaPhi)
		{							
			for(double theta = 0.0; theta <= (Math.PI * 2); theta+=deltaTheta)
			{				
				Vertex vertex = new Vertex(0,0,0);
				vertex.x = (float) (radius * Math.sin(phi) * Math.cos(theta));
				vertex.y = (float) (radius * Math.sin(phi) * Math.sin(theta));
				vertex.z = (float) (radius * Math.cos(phi));
								
			    vertices.add(vertex);
				
			    if( phi == 0 || phi == Math.PI )
				{					
					//Create only one point for the top and bottom
					index++;
					break;
				}
			    else
			    {
			    	if( phi == deltaPhi )
			    	{
			    		if( theta != (Math.PI * 2))
			    		{
			    			indexList.add(index);
			    			indexList.add((short)(index+1));
			    			indexList.add((short)(0));
			    		} 
			    		else
			    		{
			    			//indexList.add(index);
			    			//indexList.add((short)(1));
			    			//indexList.add((short)(0));
			    		}
			    	} 
			    	else
			    	{	
			    		//Create just one triangle for the first index
			    		if(theta == 0)
			    		{
			    			indexList.add(index);
			    			indexList.add((short)(index + 1));
			    			indexList.add((short)(index - levelCount));
			    		}
			    		else if(theta == (Math.PI * 2)) //Create the single end triangle
			    		{
			    			indexList.add(index);
			    			short up = (short) (index - levelCount);
			    			indexList.add(up);
			    			indexList.add((short)(up - 1));

			    		}
			    		else
			    		{
			    			indexList.add(index);
			    			short up = (short) (index - levelCount);
			    			indexList.add(up);
			    			indexList.add((short)(up - 1));

			    			indexList.add(index);
			    			indexList.add((short)(index + 1));
			    			indexList.add((short)(index - levelCount));
			    		}
			    	}		
			    	
			    	if( phi == Math.PI - deltaPhi)
			    	{
			    		if( theta != (Math.PI * 2))
			    		{
			    			indexList.add(index);
			    			indexList.add((short)(index+1));
			    			indexList.add(lastIndex);
			    		}
			    		else
			    		{
			    			//indexList.add(index);
			    			//indexList.add((short)(index - levelCount + 1));
			    			//indexList.add(lastIndex);
			    		}
			    	}
			    	
			    	index++;				
			    }
			}
        }
		
		indexCount = indexList.size();
		vertexCount = vertices.size();
		vertexBuffer = GraphicUtils.convertVertexArrayListToFloatBuffer(vertices);
		vertices.clear();
		indexBuffer = GraphicUtils.convertShortArrayListToShortBuffer(indexList);
		indexList.clear();
	}
	
	@Override
	public FloatBuffer getVertexBuffer()
	{
		vertexBuffer.position(0);
		return vertexBuffer;
	}
	
	@Override
	public int getVertexCount()
	{
		return vertexCount;
	}
	
	public void draw(GL10 gl, int drawingType)
	{
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glDrawArrays(drawingType, 0, vertexCount);  	
	}
	
	public void draw(int drawingType)
	{
	}
	
	public int findIntersections( Vertex startPoint, Vertex endPoint, ArrayList<Vertex> intersections)
	{
		int numIntersections = 0;
		
		float t0, t1;
		float B, C;
		Vertex d;
		float t;
		Vertex center = new Vertex(0, 0, 0);
		float interX, interY, interZ;
		
		intersections = new ArrayList<Vertex>();
		
		d = new Vertex(endPoint.x, endPoint.y, endPoint.z);
		d.normalize();
		
		//Get coefficiants of the polynomial equation of the sphere
		//A = d.x*d.x + d.y*d.y + d.z*d.z;
		B = 2 * (d.x *(startPoint.x - center.x) + d.y*(startPoint.y - center.y) + d.z*(startPoint.z - center.z));
		C = (float) ((startPoint.x - center.x)*(startPoint.x - center.x) + (startPoint.y - center.y)*(startPoint.y - center.y) + (startPoint.z - center.z)*(startPoint.z - center.z) - radius*radius); 
		
		//Make sure the distance to the object isn't behind the eye
		t = -(B*B - 4*C);
		
		if( t < 0)
		{
			t0 = (float) ((-B + (Math.sqrt((B*B) - (4*C))))/2);
			t1 = (float) ((-B - (Math.sqrt((B*B) - (4*C))))/2);
			
						
			//find intersection points is the closest
			if(t0 > 0)
			{
				//Solve the ray equation to get the intersection point
				interX = startPoint.x + (endPoint.x * t0);
				interY = startPoint.y + (endPoint.y * t0);
				interZ = startPoint.z + (endPoint.z * t0);
				
				intersections.add(new Vertex(interX, interY, interZ));
				numIntersections++;		
			}
			else if(t1 > 0)
			{
				interX = startPoint.x + (endPoint.x * t0);
				interY = startPoint.y + (endPoint.y * t0);
				interZ = startPoint.z + (endPoint.z * t0);
				
				intersections.add(new Vertex(interX, interY, interZ));
								
				numIntersections++;
			}
		}
		return numIntersections;
	}	
	
	public Vertex findPointOnSphere(float x, float y)
	{
		Vertex vertex = new Vertex(0,0,0);
		float dividend = (float) ( 1 + Math.pow(x, 2) + Math.pow(y, 2));
		vertex.x = (float) ((2 * x) / dividend);
		vertex.y = (float) ((2 * y) / dividend);
		vertex.z = (float) Math.abs((-1 + Math.pow(x, 2) + Math.pow(y, 2)) / dividend);
		return vertex;
	}

	@Override
	public ShortBuffer getIndexBuffer() 
	{
		return indexBuffer;
	}

	@Override
	public FloatBuffer getNormalBuffer() 
	{
		return null;
	}

	@Override
	public int getIndexCount() 
	{
		return indexCount;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void releaseVertexBuffer() 
	{
		if( vertexBuffer != null )
		{
			vertexBuffer.clear();
			vertexBuffer = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indexBuffer != null )
		{
			indexBuffer.clear();
			indexBuffer = null;
		}
	}
}