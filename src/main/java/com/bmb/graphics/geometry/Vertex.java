package com.bmb.graphics.geometry;


import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Vertex
{
	public float x;
	public float y;
	public float z;
	
	//Can be used to store generic value at this point. Passed to GL as the alpha
	public float value;
	
	public Vertex( float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vertex( float x, float y, float z, float value)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.value = value;
	}
	
	public Vertex()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
	
	public void set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void normalize()
	{
		float l = length();
		if (l > 0) {
			x /= l;
			y /= l;
			z /= l;
		}
	}
	
	public float length() 
	{
		float l = (float) Math.sqrt( (float) (Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)) );
		return l;
	}
	
	public float length(Vertex vertex)
	{
		float l = (float) Math.sqrt((float) (Math.pow((vertex.x - x), 2) + Math.pow((vertex.y - y), 2) + Math.pow((vertex.z - z), 2)));
		return l;
	}
	
	public static float length(Vertex v1, Vertex v2)
	{
		float l = (float) Math.sqrt((float) (Math.pow((v2.x - v1.x), 2) + Math.pow((v2.y - v1.y), 2) + Math.pow((v2.z - v1.z), 2)));
		return l;
	}
	
	public void cross(final Vertex v1, final Vertex v2)
	{
		x = v1.y*v2.z - v1.z*v2.y;
		y = v1.z*v2.x - v1.x*v2.z;
		z = v1.x*v2.y - v1.y*v2.x;		
	}

    public Vertex cross(Vertex v2)
    {
        Vertex result = new Vertex();
        result.x = y * v2.z - z * v2.y;
        result.y = z * v2.x - x * v2.z;
        result.z = x * v2.y - y * v2.x;

        return result;
    }
	public void dot(double value)
	{
		x *= value;
		y *= value;
		z *= value;
	}
	
	public float dot(Vertex vertex)
	{
		return (x * vertex.x) + (y * vertex.y) + (z * vertex.z);
	}
	
	public void subtract(Vertex v1)
	{
		x -= v1.x;
		y -= v1.y;
		z -= v1.z;
	}
	
	public static Vertex subtract(Vertex v1, Vertex v2)
	{
		return new Vertex( v1.x - v2.x, v1.y - v2.y, v1.z + v2.z);
	}	
	
	public static Vertex add(Vertex v1, Vertex v2) {
		return new Vertex( v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}

	public void add(Vertex v1)
	{
		x += v1.x;
		y += v1.y;
		z += v1.z;
	}

    public static void sortVerticesByDistance(List<Vertex> vertices, final Vertex point)
    {
        Collections.sort(vertices, new Comparator<Vertex>()
        {

            @Override
            public int compare(Vertex pointOne, Vertex pointTwo)
            {
                float pointOneDistance = point.length(pointOne);
                float pointTwoDistance = point.length(pointTwo);

                return Float.compare(pointOneDistance, pointTwoDistance);
            }
        });
    }

	public static Vertex findVertexOnLine(Vertex start, Vertex end, float length)
	{
		Vertex vertex = new Vertex();
		float lineLength = Vertex.length(start, end);
		float lengthRatio = length / lineLength;
				
		vertex = Vertex.subtract(end, start);
		
		vertex.dot(lengthRatio);		
		vertex.add(start);
		return vertex;
	}
	
	public String toString()
	{
		return new String(x + ", " + y + ", " + z);
	}

    @Override
    public boolean equals(Object other)
    {
        boolean equal = false;
        if( other instanceof Vertex )
        {
            Vertex otherVert = (Vertex) other;
            if( x == otherVert.x && y == otherVert.y && z == otherVert.z && value == otherVert.value )
            {
                equal = true;
            }
        }

        return equal;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + Float.floatToIntBits(x);
        hash = hash * 31 + Float.floatToIntBits(y);
        hash = hash * 31 + Float.floatToIntBits(z);
        hash = hash * 31 + Float.floatToIntBits(value);

        return hash;
    }
}
