package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import com.bmb.graphics.utils.GraphicUtils;

public class Curve implements Geometry
{
	private Vertex[] controlPoints;
	private ArrayList<Vertex> curvePoints;
	private ArrayList<Vertex> tangents;
	private ArrayList<Vertex> normals;
	private ArrayList<Double> curveLengths;
	
	public Curve(Vertex start, Vertex end)
	{
		curvePoints = new ArrayList<Vertex>();		
		tangents = new ArrayList<Vertex>();
		normals = new ArrayList<Vertex>();
		curveLengths = new ArrayList<Double>();
		curveLengths.add(0.0);
		
		controlPoints = new Vertex[2];
		controlPoints[0] = start;
		controlPoints[1] = end;
		Vertex normal = new Vertex(0, 0, 1);
		
		normal.cross(start, end);
		normal.normalize();
		
		normals.add(normal);
	}
	
	public Curve(Vertex[] controlPoints)
	{
		curvePoints = new ArrayList<Vertex>();		
		tangents = new ArrayList<Vertex>();
		normals = new ArrayList<Vertex>();
		curveLengths = new ArrayList<Double>();
		curveLengths.add(0.0);
		
		this.controlPoints = controlPoints;
		Vertex normal = new Vertex(0, 0, 1);
		normal.cross(controlPoints[0], controlPoints[controlPoints.length-1]);
		normal.normalize();
		normals.add(normal);
	}
	
	public ArrayList<Vertex> getVertices()
	{
		return curvePoints;
	}
	
	public ArrayList<Vertex> getNormals()
	{
		return normals;
	}
	
	public ArrayList<Vertex> getTangents()
	{
		return tangents;
	}
	
	public void draw(GL10 gl)
	{
		FloatBuffer vertexBuffer = GraphicUtils.convertVertexArrayListToFloatBuffer(curvePoints);
		   	   
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glDrawArrays(GL10.GL_LINE_STRIP, 0, curvePoints.size()); 
	}
	
	public Vertex getEndControlPoint()
	{
		return controlPoints[controlPoints.length-1];
	}
	
	public void createCurve()
	{
		int curveNum = 0;
		if( controlPoints != null && controlPoints.length > 4)
		{
			curvePoints.clear();
			//Start off with the first four points
			Vertex [] points = new Vertex[4];
			
			//Loop through the rest of the points reuse last three points to make nice smooth curve
			for( int i = 0; i < controlPoints.length; i++ )
			{
				/* get the next point */
				Vertex pt = controlPoints[i];				
				
				//loop through 4 points at a time to get the spline control points
				if(curveNum == 0)
					points[curveNum] = pt;
				else
				points[curveNum] = pt;

				if(curveNum == 3)
				{		
					interpolateCurve(points);
					points[0] = points[1];
					points[1] = points[2];
					points[2] = points[3];
					curveNum = 3;
				}
				else
					curveNum++;
 			}			
			
			curvePoints.add(0, controlPoints[0]);
			curvePoints.add(getEndControlPoint());
		}
		else if( controlPoints != null)
		{
			for(Vertex vertex : controlPoints)
			{
				curvePoints.add(vertex);
			}
		}
	}
	
	private void interpolateCurve(Vertex points[])
	{
		//Four vectors to hold the four control points
		Vertex point0 = points[0];
		Vertex point1 = points[1];
		Vertex point2 = points[2];
		Vertex point3 = points[3];
		//Tension on the curve
		float t = 0.5f;
		//The u value to calculate the spline points
		float u;
		//The transformation matrix
		float M[] = {0, 1.0f, 0, 0, -t, 0, t, 0, (2.0f * t), (t-3.0f), (3.0f - 2.0f * t), -t, -t, (2 - t), (t - 2), t}; 
		//The controlvalues
		Vertex c1 = new Vertex(0,0,0);
		Vertex c2 = new Vertex(0,0,0);
		Vertex c3 = new Vertex(0,0,0);
		Vertex c4 = new Vertex(0,0,0);
		//Temporary variables to hold the splinepoint and tangent until they are pushed on to the vector
		
		double estValue; //variable to hold curve lengths
		
		//Compute control points based on the transformation matrix
		c1.x = point1.x;
		c2.x = M[4] * point0.x + M[6] * point2.x;
		c3.x = M[8] * point0.x + M[9] * point1.x + M[10] * point2.x + M[11] * point3.x;
		c4.x = M[12] * point0.x + M[13] * point1.x + M[14] * point2.x + M[15] * point3.x;
		
		c1.y = point1.y;
		c2.y = M[4] * point0.y + M[6] * point2.y;
		c3.y = M[8] * point0.y + M[9] * point1.y + M[10] * point2.y + M[11] * point3.y;
		c4.y = M[12] * point0.y + M[13] * point1.y + M[14] * point2.y + M[15] * point3.y;

		c1.z = point1.z;
		c2.z = M[4] * point0.z + M[6] * point2.z;
		c3.z = M[8] * point0.z + M[9] * point1.z + M[10] * point2.z + M[11] * point3.z;
		c4.z = M[12] * point0.z + M[13] * point1.z + M[14] * point2.z + M[15] * point3.z;
		
		for(u = 0.0f; u < 1.0; u += .1)
		{
			Vertex splinepoint = new Vertex(0,0,0);
			Vertex tangent = new Vertex(0,0,0);
			
			//Multiply by the u vector and then add to curvepoints vector
			splinepoint.x = (float) (1 * c1.x + u * c2.x + Math.pow(u, 2) * c3.x + Math.pow(u, 3) * c4.x);
			splinepoint.y = (float) (1 * c1.y + u * c2.y + Math.pow(u, 2) * c3.y + Math.pow(u, 3) * c4.y);
			splinepoint.z = (float) (1 * c1.z + u * c2.z + Math.pow(u, 2) * c3.z + Math.pow(u, 3) * c4.z);
			//Log.e(context.getString(R.string.log_tag), "Splinepoint x: " + splinepoint.x + " y: " + splinepoint.y + " z: " + splinepoint.z );

			curvePoints.add(splinepoint);	
			
			//x = 1 * c1 + u * c2 + u^2 * c3 + u^3 * c4
			//x' = 1 * c2 + 2u * c3 + 3 * u^2 * c4
			//x'' = 1 * c2 + 2 * c3 + 6 * u * c4
			
			//Compute the tangent by taking the derivative of the spline point calculation
			tangent.x = (float) (1 * c2.x + 2*u * c3.x + 3 * Math.pow(u, 2) * c4.x);
			tangent.y = (float) (1 * c2.y + 2*u * c3.y + 3 * Math.pow(u, 2) * c4.y);
			tangent.z = (float) (1 * c2.z + 2*u * c3.z + 3 * Math.pow(u, 2) * c4.z);
			
			/*
			Vertex normal = new Vertex(0,0,0);
			normal.x = (float)(1 * c2.x + 2 * c3.x + 6 * u * c4.x);
			normal.y = (float)(1 * c2.y + 2 * c3.y + 6 * u * c4.y);
			normal.z = (float)(1 * c2.z + 2 * c3.z + 6 * u * c4.z);
			*/
	
			//Normalize the tangent and then offset it by the spline point to move 
			//it to the correct location on the track then add to the tangents vector
			tangent.normalize();
			tangent = Vertex.add(tangent, splinepoint);

			//Compute the normal based on the tangents
			Vertex normal = new Vertex(tangent.y, tangent.x, 0);
			if( tangents.size() > 1)
			{
				//normal.cross(tangents.get(tangents.size()-1), tangent);
			}
			normal.normalize();

			//Log.e("BMB", "N: " + normal + " t: " + tangent + " N*t: " + test);

			
			normal = Vertex.add(normal, splinepoint);

			tangents.add(tangent);
			normals.add(normal);
			
			estValue = (float) Math.abs(Math.sqrt(Math.pow(splinepoint.x, 2) + Math.pow(splinepoint.y, 2) + Math.pow(splinepoint.z, 2))); 
			estValue += curveLengths.get(curveLengths.size()-1);
			curveLengths.add(estValue);
		}
	}

	@Override
	public FloatBuffer getVertexBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ShortBuffer getIndexBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FloatBuffer getNormalBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getVertexCount() {
		return curvePoints.size();
	}

	@Override
	public int getIndexCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public FloatBuffer getTextureCoordBuffer(TextureType type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTextureCoordCount(TextureType type) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void releaseVertexBuffer() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void releaseIndexBuffer() {
		// TODO Auto-generated method stub
		
	}
}