package com.bmb.graphics.geometry;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import com.bmb.graphics.utils.GraphicUtils;

public class TaperedTube implements Geometry
{
    FloatBuffer vertices;
    ShortBuffer indices;
    float yResolution =  .25f;
    float xzResolution = 20f;
    private static double DEG = Math.PI/180;

    private int vertexCount = 0;
    private int indexCount = 0;

    public TaperedTube(Float baseWidth, Float midWidth, float length)
    {
        buildTube(baseWidth, midWidth, length);
    }

    private void buildTube(float baseWidth, float midWidth, float length)
    {
        //Convert resolution to radians
        xzResolution = (float) (xzResolution * DEG);
        //length = 2;
        ArrayList<Vertex> vertexList = new ArrayList<Vertex>();
        ArrayList<Short> indexList = new ArrayList<Short>();
        short levelCount = (short) ( (Math.PI * 2) / xzResolution  + 1 );

        float heightSteps = length / yResolution;
        float midPoint = heightSteps / 2.0f;
        float scaleSize = (baseWidth - midWidth)/midPoint;

        short index = 0;
        int heightLevel = 0;

        for(float y = 0.0f; y <= length; y += yResolution)
        {
            for(float j = 0.0f; j < Math.PI * 2; j += xzResolution)
            {
                float width = baseWidth - (heightLevel * scaleSize);
                float x = (float) (width * Math.cos(j));
                float z = (float) (width * Math.sin(j));
                vertexList.add(new Vertex(x, y, z));

                if( y != length && y + yResolution > length)
                {
                    y = length - yResolution;
                }

                //Don't add indices for the top row
                if( y + yResolution <= length )
                {
                    //Create just one triangle for the first index
                    if(j == 0)
                    {
                        indexList.add(index);
                        indexList.add((short)(index + 1));
                        indexList.add((short)(index + levelCount));
                    }
                    else if(j == levelCount - 1) //Create the single end triangle
                    {
                        indexList.add(index);
                        short up = (short) (index + levelCount);
                        indexList.add(up);
                        indexList.add((short)(up - 1));
                    }
                    else
                    {
                        indexList.add(index);
                        short up = (short) (index + levelCount);
                        indexList.add(up);
                        indexList.add((short)(up - 1));

                        indexList.add(index);
                        indexList.add((short)(index + 1));
                        indexList.add((short)(index + levelCount));
                    }
                }
                index++;
            }
            if( y <= length/2 )
            {
                heightLevel++;
            }
            else
            {
                heightLevel--;
            }
        }

        indexCount = indexList.size();
        vertexCount = vertexList.size();
        vertices = GraphicUtils.convertVertexArrayListToFloatBuffer(vertexList);
        vertexList.clear();
        indices = GraphicUtils.convertShortArrayListToShortBuffer(indexList);
        indexList.clear();
    }

    @Override
    public FloatBuffer getVertexBuffer()
    {
        return vertices;
    }

    @Override
    public ShortBuffer getIndexBuffer()
    {
        return indices;
    }

    public void setXZResolution(float res)
    {
        xzResolution = res;
    }

    public void setYResolution(float res)
    {
        yResolution = res;
    }

    @Override
    public FloatBuffer getNormalBuffer()
    {
        return null;
    }

    @Override
    public FloatBuffer getTextureCoordBuffer(TextureType type)
    {
        return null;
    }

    @Override
    public int getVertexCount()
    {
        return vertexCount;
    }

    @Override
    public int getIndexCount()
    {
        return indexCount;
    }

    @Override
    public int getTextureCoordCount(TextureType type)
    {
        return 0;
    }

	@Override
	public void releaseVertexBuffer() 
	{
		if( vertices != null )
		{
			vertices.clear();
			vertices = null;
		}
	}

	@Override
	public void releaseIndexBuffer() 
	{
		if( indices != null )
		{
			indices.clear();
			indices = null;
		}
	}
}