package com.bmb.graphics.shader;

import com.bmb.graphic.utils.R;

public class BasicFragmentShader implements Shader 
{
	private String shaderSource;

	public BasicFragmentShader()
	{
		shaderSource = ShaderLoader.loadShader(R.raw.simple_frag);
	}
	
	@Override
	public String getSource() {
		return shaderSource;
	}

	@Override
	public void setSource(String source) {
		shaderSource = source;
	}

	@Override
	public void setupHandlers(int program) {
		
	}

	@Override
	public int getPositionHandle() {
		return -1;
	}

	@Override
	public int getNormalHandle() {
		return -1;
	}

	@Override
	public int getModelViewProjectionMatrixHandle() {
		return -1;
	}

	@Override
	public int getModelViewMatrixHandle() {
		return -1;
	}
	
	@Override
	public int getValueHandle(String value) {
		return -1;
	}

	@Override
	public void bindTextures() {
	}

	@Override
	public int getNormalMatrixHandle() {
		return -1;
	}
	
}