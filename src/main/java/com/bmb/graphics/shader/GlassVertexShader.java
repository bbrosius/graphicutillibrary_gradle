package com.bmb.graphics.shader;

import android.opengl.GLES20;

import com.bmb.graphic.utils.R;

public class GlassVertexShader implements Shader
{
	private String shaderSource;
    private int positionHandle;
    private int normalHandle;
    private int modelViewProjectionHandle;
    private int modelViewMatrixHandle;
    
    public GlassVertexShader()
    {
    	shaderSource = ShaderLoader.loadShader(R.raw.glass_vert);
    }
    
	@Override
	public String getSource()
	{
		return shaderSource;
	}

	@Override
	public void setSource(String source)
	{
		shaderSource = source;		
	}

	@Override
	public void setupHandlers(int program)
	{
		positionHandle = GLES20.glGetAttribLocation(program, "position");
		modelViewProjectionHandle = GLES20.glGetUniformLocation(program, "modelViewProjectionMatrix");
		modelViewMatrixHandle = GLES20.glGetUniformLocation(program, "modelViewMatrix");
	}

	@Override
	public int getPositionHandle() {
		return positionHandle;
	}

	@Override
	public int getNormalHandle() {
		return normalHandle;
	}

	@Override
	public int getModelViewProjectionMatrixHandle() {
		return modelViewProjectionHandle;
	}

	@Override
	public int getModelViewMatrixHandle() {
		return modelViewMatrixHandle;
	}

	@Override
	public int getValueHandle(String value) {
		return -1;
	}

	@Override
	public void bindTextures() {
		
	}

	@Override
	public int getNormalMatrixHandle() {
		return -1;
	}
	
}