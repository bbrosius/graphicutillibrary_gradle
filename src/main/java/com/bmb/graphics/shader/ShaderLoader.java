package com.bmb.graphics.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.res.Resources.NotFoundException;
import android.util.Log;

import com.bmb.graphic.utils.R;
import com.bmb.utils.Utils;

public class ShaderLoader
{
	public static String loadShader(int resourceID)
	{
		InputStream resource = null;
		try {
			if( Utils.getContext() != null )
			{
				resource = Utils.getContext().getResources().openRawResource(resourceID);
			}
		} 
		catch (NotFoundException e)
		{
			Log.e(Utils.getContext().getString(R.string.log_tag), e.getMessage());
		}	
		
		StringBuilder shaderString = new StringBuilder();

		if( resource != null )
		{
			BufferedReader r = new BufferedReader(new InputStreamReader(resource));
			String line;
			try
			{
				while ((line = r.readLine()) != null)
				{
				    shaderString.append(line);
				    //Append a new line character after every line. This helps keep source form. 
				    //And prevents errors when using c++ style comments. 
				    shaderString.append("\n");
			    }
			} 
			catch (IOException e)
			{
				Log.e(Utils.getContext().getString(R.string.log_tag), "Error reading vert file.");
			}
		}
		return shaderString.toString();
	}
	
}