package com.bmb.graphics.shader;

import android.opengl.GLES20;

import com.bmb.graphic.utils.R;


public class BasicVertexShader implements Shader
{
	private String shaderSource;
    private int positionHandle;
    private int modelViewProjectionHandle;
	    	  
    public BasicVertexShader()
    {
    	shaderSource = ShaderLoader.loadShader(R.raw.simple_vert);
    }
		
	@Override
	public String getSource() {
		return shaderSource;
	}

	@Override
	public void setSource(String source) {
		shaderSource = source;		
	}

	@Override
	public void setupHandlers(int program) {
		positionHandle = GLES20.glGetAttribLocation(program, "position");
		
		modelViewProjectionHandle = GLES20.glGetUniformLocation(program, "modelViewProjectionMatrix");

	}

	@Override
	public int getPositionHandle() {
		return positionHandle;
	}

	@Override
	public int getNormalHandle() {
		return -1;
	}

	@Override
	public int getModelViewProjectionMatrixHandle() {
		return modelViewProjectionHandle;
	}
	
	@Override
	public int getModelViewMatrixHandle() {
		return -1;
	}

	@Override
	public int getValueHandle(String value) {
		return -1;
	}

	@Override
	public void bindTextures() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getNormalMatrixHandle() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}