package com.bmb.graphics.shader;


import android.opengl.GLES20;
import android.util.Log;

import com.bmb.graphic.utils.R;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.utils.Utils;

public class GLESProgramCreator
{
	private int program;
	private Shader vertexShader;
	private Shader fragmentShader;
	private int vertexShaderID;
	private int fragmentShaderID;

	public GLESProgramCreator(Shader vertexShader, Shader fragmentShader)
	{
		this.vertexShader = vertexShader;
		this.fragmentShader = fragmentShader;
	}
	
	public void setFragmentShader(Shader shader)
	{
		this.fragmentShader = shader;
	}
	
	public void setVertexShader(Shader shader)
	{
		this.vertexShader = shader;
	}
	
	public int getFragmentShader()
	{
		return fragmentShaderID;
	}
	
	public int getVertexShader()
	{
		return vertexShaderID;
	}
	
	public int createProgram()
	{
		if( vertexShader != null && fragmentShader != null )
		{
			vertexShaderID = loadShader(GLES20.GL_VERTEX_SHADER, vertexShader);
			
	        fragmentShaderID = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
	        
	        program = GLES20.glCreateProgram();
	        
	        if (vertexShaderID != 0 && fragmentShaderID != 0) {
	            GLES20.glAttachShader(program, vertexShaderID);
	            GraphicUtils.checkGlError("glAttachShader vertexShader");
	            GLES20.glAttachShader(program, fragmentShaderID); 
	            GraphicUtils.checkGlError("glAttachShader fragmentShader");
	            GLES20.glLinkProgram(program);
	            int[] linkStatus = new int[1];
	            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
	            if (linkStatus[0] != GLES20.GL_TRUE) {
	            	if( com.bmb.utils.Utils.getContext() != null )
	            	{
		                Log.e(com.bmb.utils.Utils.getContext().getString(R.string.log_tag), "Could not link program: ");
		                Log.e(com.bmb.utils.Utils.getContext().getString(R.string.log_tag), GLES20.glGetProgramInfoLog(program));
	            	}
	                GLES20.glDeleteProgram(program);
	                program = 0;
	            }
	        }	 
	        else 
	        {
	        	program = 0;
	        }
	        
	        vertexShader.setupHandlers(program);
	        fragmentShader.setupHandlers(program);
		}
        return program;
	}
	
	private int loadShader(int shaderType, Shader shader)
	{
		int shaderID = GLES20.glCreateShader(shaderType);
	    if( shaderID != 0 && shader != null ) 
	    {
	        GLES20.glShaderSource(shaderID, shader.getSource());
	        GLES20.glCompileShader(shaderID);

	        int[] compiled = new int[1];
	        GLES20.glGetShaderiv(shaderID, GLES20.GL_COMPILE_STATUS, compiled, 0);
	        if (compiled[0] == 0) {
	            Log.e(Utils.getContext().getString(R.string.log_tag), "Could not compile shader " + shaderType + ": " + shader);
	            Log.e(Utils.getContext().getString(R.string.log_tag), GLES20.glGetShaderInfoLog(shaderID));
	            GLES20.glDeleteShader(shaderID);
	            shaderID = 0;
	        }
	    }
	    return shaderID;
	}
	
	
}