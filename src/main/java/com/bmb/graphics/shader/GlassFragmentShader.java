package com.bmb.graphics.shader;

import android.opengl.GLES20;

import com.bmb.graphic.utils.R;

public class GlassFragmentShader implements Shader
{
	private String source;
	private int lightPositionHandle;
    private int eyePositionHandle;

	public GlassFragmentShader()
	{
		source = ShaderLoader.loadShader(R.raw.glass_frag);
	}
	
	@Override
	public String getSource()
    {
		return source;
	}

	@Override
	public void setSource(String source)
    {
		this.source = source;
	}

	@Override
	public void setupHandlers(int program)
    {
		lightPositionHandle = GLES20.glGetUniformLocation(program, "lightPosition");
        eyePositionHandle = GLES20.glGetUniformLocation(program, "eyePos");
	}

	@Override
	public int getPositionHandle()
    {
		return -1;
	}

	@Override
	public int getNormalHandle()
    {
		return -1;
	}

	@Override
	public int getModelViewProjectionMatrixHandle()
    {
		return -1;
	}

	@Override
	public int getModelViewMatrixHandle()
    {
		return -1;
	}

	@Override
	public int getNormalMatrixHandle()
    {
		return -1;
	}

	@Override
	public int getValueHandle(String value)
    {
		int handle = -1;
		if( value.equalsIgnoreCase("lightPosition"))
        {
			handle = lightPositionHandle;
		}
        else if( value.equalsIgnoreCase("eyePos"))
        {
            handle = eyePositionHandle;
        }
		
		return handle;
	}

	@Override
	public void bindTextures()
    {
		
	}
	
}