package com.bmb.graphics.shader;

public interface Shader
{
	/**
	 * Gets the glsl code for the shader contained in a string.
	 * @return A string of the glsl shader information.
	 */
	public String getSource();
	
	/**
	 * Sets the source code for the shader. Code should be contained in a single string.
	 * @param source The string containing the source code.
	 */
	public void setSource(String source);
	
	/**
	 * Used to set all the handles and link them to the program. 
	 * @param program The handle to the program to link. 
	 */
	public void setupHandlers(int program);
	
	/**
	 * Returns the handle to the position attribute. 
	 * @return The integer representing the handle that should be used to set the value.
	 */
	public int getPositionHandle();
	
	/**
	 * Returns the handle to the normal attribute.
	 * @return The integer representing the handle that should be used to set the value.
	 */
	public int getNormalHandle();
	
	/**
	 * Returns the handle to the model view matrix uniform.
	 * @return The integer representing the handle that should be used to set the matrix for the program. 
	 */
	public int getModelViewProjectionMatrixHandle();
	
	/**
	 * Returns the handle to the model view matrix uniform.
	 * @return The integer representing the handle that should be used to set the matrix for the program.
	 */
	public int getModelViewMatrixHandle();
	
	/**
	 * Returns the handle to the normal matrix uniform.
	 * @return The integer representing the handle that should be used to set the matrix for the program.
	 */
	public int getNormalMatrixHandle();
	
	/**
	 * Gets the handle for the value specified by the string
	 * @param the name of the handle to retrieve. 
	 * @return returns the handle or -1 if the handle isn't found
	 */
	public int getValueHandle(String value);
	
	/**
	 * Must be called if using textures in the shader. Will bind textures to the shader. 
	 */
	public void bindTextures();
}