package com.bmb.graphics.shader;

import android.opengl.GLES20;

import com.bmb.graphic.utils.R;

public class BasicTextureVertexShader implements Shader 
{
	
	private int modelViewProjectionMatrixHandle;
	private int positionHandle;
	private int vertexTextureHandle;
	
	private String source;
	
	public BasicTextureVertexShader()
	{
		source = ShaderLoader.loadShader(R.raw.simple_texture_vert);
	}
	
	@Override
	public String getSource() {
		return source;
	}

	@Override
	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public void setupHandlers(int program) {
		positionHandle = GLES20.glGetAttribLocation(program, "position");
		
		modelViewProjectionMatrixHandle = GLES20.glGetUniformLocation(program, "modelViewProjectionMatrix");
		
		vertexTextureHandle = GLES20.glGetAttribLocation(program, "vertexTextureCoord");
	}

	@Override
	public int getPositionHandle() {
		return positionHandle;
	}

	@Override
	public int getNormalHandle() {
		return -1;
	}

	@Override
	public int getModelViewProjectionMatrixHandle() {
		return modelViewProjectionMatrixHandle;
	}

	@Override
	public int getModelViewMatrixHandle() {
		return -1;
	}

	@Override
	public int getNormalMatrixHandle() {
		return -1;
	}

	@Override
	public int getValueHandle(String value) {
		int handle = -1;
		if(value.equalsIgnoreCase("vertexTexture"))
		{
			handle = vertexTextureHandle;
		}
		return handle;
	}

	@Override
	public void bindTextures() {
		
	}
	
}