package com.bmb.graphics.shader;

import android.opengl.GLES20;

import com.bmb.graphic.utils.R;

public class BasicTextureFragmentShader implements Shader
{
	private int textureHandle;
	private String source;
	
	public BasicTextureFragmentShader()
	{
		source = ShaderLoader.loadShader(R.raw.simple_texture_frag);
	}
	
	@Override
	public String getSource() {
		return source;
	}

	@Override
	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public void setupHandlers(int program) {
		textureHandle = GLES20.glGetUniformLocation(program, "texture");
	}

	@Override
	public int getPositionHandle() {
		return -1;
	}

	@Override
	public int getNormalHandle() {
		return -1;
	}

	@Override
	public int getModelViewProjectionMatrixHandle() {
		return -1;
	}

	@Override
	public int getModelViewMatrixHandle() {
		return -1;
	}

	@Override
	public int getNormalMatrixHandle() {
		return -1;
	}

	@Override
	public int getValueHandle(String value) {
		int handle = -1;
		if(value.equalsIgnoreCase("texture"))
		{
			handle = textureHandle;
		}
		return handle;
	}

	@Override
	public void bindTextures() {
		
	}
	
}