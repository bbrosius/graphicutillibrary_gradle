package com.bmb.graphics.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import com.bmb.graphic.utils.R;
import com.bmb.utils.Utils;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;

/**
 * Created by Ben on 4/15/2014.
 */
public class BitmapLoaderTask extends AsyncTask<Uri, Void, Bitmap>
{
    private final WeakReference<ImageView> imageViewReference;
    private final int height;
    private final int width;
    private Uri imagePath = null;

    public BitmapLoaderTask(ImageView imageView, int width, int height)
    {
        imageViewReference = new WeakReference<ImageView>(imageView);
        this.height = height;
        this.width = width;
    }

    @Override
    protected Bitmap doInBackground(Uri... imageUri)
    {
        // First we get the the dimensions of the file on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        imagePath = imageUri[0];
        options.inJustDecodeBounds = true;
        try
        {
            BitmapFactory.decodeStream(Utils.getContext().getContentResolver().openInputStream(imagePath), null, options);
        }
        catch( FileNotFoundException e )
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error loading bitmap: " + e.getMessage());
        }

        int sampleSize = GraphicUtils.calculateInSampleSize(options, width, height);

        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;

        Bitmap loadedBitmap = null;
        try
        {
            loadedBitmap = BitmapFactory.decodeStream(Utils.getContext().getContentResolver().openInputStream(imagePath), null, options);
        }
        catch( FileNotFoundException e )
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error loading bitmap: " + e.getMessage());
        }

        GraphicUtils.addBitmapToMemoryCache(imagePath, loadedBitmap);

        return loadedBitmap;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            final BitmapLoaderTask bitmapWorkerTask =
                    getBitmapLoaderTask(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public static class AsyncDrawable extends BitmapDrawable
    {
        private final WeakReference<BitmapLoaderTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapLoaderTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapLoaderTask>(bitmapWorkerTask);
        }

        public BitmapLoaderTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    public static boolean cancelPotentialWork(Uri data, ImageView imageView) {
        final BitmapLoaderTask bitmapLoaderTask = getBitmapLoaderTask(imageView);

        if (bitmapLoaderTask != null) {
            final Uri bitmapPath = bitmapLoaderTask.imagePath;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapPath == null || bitmapPath != data) {
                // Cancel previous task
                bitmapLoaderTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private static BitmapLoaderTask getBitmapLoaderTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }
}
