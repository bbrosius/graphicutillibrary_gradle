package com.bmb.graphics.utils;

import com.bmb.graphics.geometry.Vertex;

public class Light
{
	public Vertex position;
	public Vertex ambient;
	public Vertex diffuse;
	public Vertex specular;
	public Vertex emissive;
	
	public Light()
	{
	}
	
	public Light( Vertex position, Vertex ambient, Vertex diffuse, Vertex specular, Vertex emissive)
	{
		this.position = position;
		this.ambient = ambient;
		this.diffuse = diffuse;
		this.specular = specular;
		this.emissive = emissive;
	}
	
}