package com.bmb.graphics.utils;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL11;

import android.content.Context;
import android.opengl.GLDebugHelper;
import android.util.Log;
import android.view.SurfaceHolder;

import com.bmb.graphic.utils.R;

public class GLHelper
{
	private Object surface;
	 // main OpenGL variables
    private GL11 gl;
    private EGL10 egl;
    private EGLDisplay eglDisplay;
    private EGLConfig eglConfig;
    private EGLSurface eglSurface;
    private EGLContext eglContext;
    private int[] eglConfigSpec = { EGL10.EGL_RED_SIZE, 5, 
            EGL10.EGL_GREEN_SIZE, 6, EGL10.EGL_BLUE_SIZE, 5, 
            EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_RENDERABLE_TYPE, 4, EGL10.EGL_NONE };
    
    private boolean debug;
   
    private Context context;
	
    public GLHelper(Object holder, Context context, boolean useDebug) 
	{
		this.surface = holder;
		this.context = context;
		debug = useDebug;
	}
	
	public GL11 createGLSurface() throws Exception
	{
		initEGL();			
		return gl;
	}
	
	public void swapEGLBuffers()
	{
		egl.eglSwapBuffers(eglDisplay, eglSurface);
	}
	
	public boolean makeEGLCurrent()
	{
		return egl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
	}
	
	public void initEGL() throws Exception 
	{
		egl = (EGL10) EGLContext.getEGL(); 
	    if (egl == null)
        {
            throw new Exception("Couldn't get EGL");
        }
        
        eglDisplay = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (eglDisplay == null)
        {
            throw new Exception("Couldn't get display for GL");
        }

        int[] curGLVersion = new int[2];
        egl.eglInitialize(eglDisplay, curGLVersion);

        EGLConfig[] configs = new EGLConfig[1];
        int[] num_config = new int[1];
        egl.eglChooseConfig(eglDisplay, eglConfigSpec, configs, 1,
                num_config);
        //eglConfig = configs[0];
        eglConfig = chooseConfig(egl, eglDisplay);
        
        eglSurface = egl.eglCreateWindowSurface(eglDisplay, eglConfig, surface, null);
     
        if (eglSurface == null)
        {
            throw new Exception("Couldn't create new surface");
        }
        
        int[] attrib_list = {0x3098, 2, EGL10.EGL_NONE};
        eglContext = egl.eglCreateContext(eglDisplay, eglConfig,
                EGL10.EGL_NO_CONTEXT, attrib_list);
       
        if (eglContext == null)
        {
            throw new Exception("Couldn't create new context");
        }


        if (!egl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext))
        {
            throw new Exception("Failed to eglMakeCurrent");
        }
       
        
        if( debug )
        {
        	gl = (GL11) GLDebugHelper.wrap(eglContext.getGL(),
                GLDebugHelper.CONFIG_CHECK_GL_ERROR
                        | GLDebugHelper.CONFIG_CHECK_THREAD
                        | GLDebugHelper.CONFIG_LOG_ARGUMENT_NAMES, null);
        	
        	egl = (EGL10) GLDebugHelper.wrap(egl,  GLDebugHelper.CONFIG_CHECK_GL_ERROR
                        | GLDebugHelper.CONFIG_CHECK_THREAD
                        | GLDebugHelper.CONFIG_LOG_ARGUMENT_NAMES, null);
        }
        else
        {
        	gl = (GL11) eglContext.getGL();
        }
        
        
        if (gl == null)
        {
            throw new Exception("Failed to get GL");
        }
	}
	
	public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display)
	{
		int[] num_config = new int[1];
		if (!egl.eglChooseConfig(display, eglConfigSpec, null, 0, num_config))
		{
			throw new IllegalArgumentException("eglChooseConfig failed");
		}

		int numConfigs = num_config[0];

		if (numConfigs <= 0)
		{
			throw new IllegalArgumentException("No configs match configSpec");
		}

		EGLConfig[] configs = new EGLConfig[numConfigs];
		if (!egl.eglChooseConfig(display, eglConfigSpec, configs, numConfigs,
				num_config))
		{
			throw new IllegalArgumentException("eglChooseConfig#2 failed");
		}
		EGLConfig config = chooseConfig(egl, display, configs);
		if (config == null)
		{
			throw new IllegalArgumentException("No config chosen");
		}
		return config;
	}
	
	private int[] mValue;
	// Subclasses can adjust these values:
	protected int mRedSize = 5;
	protected int mGreenSize = 6;
	protected int mBlueSize = 5;
	protected int mAlphaSize = 0;
	protected int mDepthSize = 16;
	protected int mStencilSize = 0;
	
	public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display,
			EGLConfig[] configs)
	{
		for (EGLConfig config : configs)
		{
			int d = findConfigAttrib(egl, display, config,
					EGL10.EGL_DEPTH_SIZE, 0);
			int s = findConfigAttrib(egl, display, config,
					EGL10.EGL_STENCIL_SIZE, 0);
			if ((d >= mDepthSize) && (s >= mStencilSize))
			{
				int r = findConfigAttrib(egl, display, config, EGL10.EGL_RED_SIZE, 0);
				int g = findConfigAttrib(egl, display, config, EGL10.EGL_GREEN_SIZE, 0);
				int b = findConfigAttrib(egl, display, config, EGL10.EGL_BLUE_SIZE, 0);
				int a = findConfigAttrib(egl, display, config, EGL10.EGL_ALPHA_SIZE, 0);
				if ((r == mRedSize) && (g == mGreenSize) && (b == mBlueSize)
						&& (a == mAlphaSize)) {
					return config;
				}
			}
		}
		return null;
	}

	private int findConfigAttrib(EGL10 egl, EGLDisplay display,
			EGLConfig config, int attribute, int defaultValue)
	{
		mValue = new int[1];
		if (egl.eglGetConfigAttrib(display, config, attribute, mValue))
		{
			return mValue[0];
		}
		return defaultValue;
	}
	
	public GL11 updateSurface(SurfaceHolder holder) throws Exception
	{
		egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, eglContext);
		egl.eglDestroySurface(eglDisplay, eglSurface);
		eglSurface = null;
		
		eglSurface = egl.eglCreateWindowSurface(eglDisplay, eglConfig, holder, null);
        if(!egl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext))
        {
            throw new Exception("Failed to eglMakeCurrent");
        }

        return gl;           
	}
	
	public void destroyGL() 
	{
		if( egl != null )
		{
			egl.eglMakeCurrent(eglDisplay, EGL10.EGL_NO_SURFACE,
	                    EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
			egl.eglDestroySurface(eglDisplay, eglSurface);
			egl.eglDestroyContext(eglDisplay, eglContext);
	        //egl.eglDestroyContext(eglDisplay, eglContext);
	        egl.eglTerminate(eglDisplay);
		}
		gl = null;
		egl = null;
        Log.i(context.getString(R.string.log_tag), "GL finished");
	}
}