package com.bmb.graphics.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.content.res.Resources.NotFoundException;
import android.util.Log;

import com.bmb.graphic.utils.R;
import com.bmb.graphics.geometry.ObjectFileGeometry;
import com.bmb.graphics.geometry.Vertex;
import com.bmb.utils.Utils;

public class ObjectFileLoader 
{
	/**
	 * Loads an object file from the given resource
	 * Object file contains vertices followed by normals, followed by texture coordinates(if they exist) followed by faces
	 * The general format is 
	 * v x y z
	 * vn x y z
	 * f v1 v2 v3 v4
	 * @param res The raw resource of the file. 
	 * @return Returns an {@link ObjectFileGeometry} of the object in the file
	 */
	public static ObjectFileGeometry loadObjectFromFile(int res)
	{
		ObjectFileGeometry objectFileObject = null;
		
		ArrayList<Float> vertices = new ArrayList<Float>();
		ArrayList<Short> indices = new ArrayList<Short>();
		ArrayList<Float> normals = new ArrayList<Float>();
		
		float minX = Float.MAX_VALUE;
		float maxX = Float.MIN_VALUE;
		
		float minY = Float.MAX_VALUE;
		float maxY = Float.MIN_VALUE;
		
		float minZ = Float.MAX_VALUE;
		float maxZ = Float.MIN_VALUE;
		
		InputStream resource = null;
		
		try {
			resource = Utils.getContext().getResources().openRawResource(res);
		} 
		catch (NotFoundException e)
		{
			Log.e(Utils.getContext().getString(R.string.log_tag), e.getMessage());
		}	
		
		if( resource != null )
		{
			BufferedReader r = new BufferedReader(new InputStreamReader(resource));
			String line;
			try
			{
				while( (line = r.readLine()) != null )
				{
				    StringTokenizer tokenizer = new StringTokenizer(line);
				    
				    //Type will be either v, vn, or f
				    String rowType = tokenizer.nextToken();
				    if(rowType.equalsIgnoreCase("v"))
				    {
				    	//Read the three vertices in
				    	float x = Float.parseFloat(tokenizer.nextToken());
				    	if( x > maxX )
				    	{
				    		maxX = x;
				    	}
				    	
				    	if( x < minX )
				    	{
				    		minX = x;
				    	}
				    	vertices.add(x);
				    	
				    	float y = Float.parseFloat(tokenizer.nextToken());
				    	if( y > maxY )
				    	{
				    		maxY = y;
				    	}
				    	
				    	if( y < minY )
				    	{
				    		minY = y;
				    	}
				    	vertices.add(y);
				    	
				    	float z = Float.parseFloat(tokenizer.nextToken());
				    	if( z > maxZ )
				    	{
				    		maxZ = z;
				    	}
				    	if( z < minZ )
				    	{
				    		minZ = z;
				    	}
				    	vertices.add(z);
				    } 
				    else if(rowType.equalsIgnoreCase("vn"))
				    {
				    	//Normal might not be normalized, so create a vertex normalize it then add it to list
				    	Vertex vertex = new Vertex(Float.parseFloat(tokenizer.nextToken()),
				    			Float.parseFloat(tokenizer.nextToken()), Float.parseFloat(tokenizer.nextToken()));
				    	vertex.normalize();
				    	normals.add(vertex.x);
				    	normals.add(vertex.y);
				    	normals.add(vertex.z);
				    }
				    else if(rowType.equalsIgnoreCase("f"))
				    {
				    	//Face can be defined one of three ways
				    	//v v v
				    	//v/vt v/vt v/vt
				    	//v/vt/vn v/vt/vn v/vt/vn
				    	//So split the token with another tokenizer
				    	short face[] = new short[3];
				    	
				    	String faceToken = tokenizer.nextToken();				    	
				    	if( !faceToken.contains("/") )
				    	{
				    		face[0] = (short) (Short.parseShort(faceToken)-1);
				    		indices.add(face[0]);
				    	}
				    	else
				    	{
				    		StringTokenizer faceTokenizer = new StringTokenizer(faceToken, "/");
				    		face[0] = (short) (Short.parseShort(faceTokenizer.nextToken())-1);
				    		indices.add(face[0]);
				    		//TODO implement face texture and face normal params
				    	}	
				    	
				    	faceToken = tokenizer.nextToken();				    	
				    	if( !faceToken.contains("/") )
				    	{
				    		indices.add((short) (Short.parseShort(faceToken)-1));
				    	}
				    	else
				    	{
				    		StringTokenizer faceTokenizer = new StringTokenizer(faceToken, "/");
				    		indices.add((short) (Short.parseShort(faceTokenizer.nextToken())-1));
				    		//TODO implement face texture and face normal params
				    	}	
				    	
				    	faceToken = tokenizer.nextToken();				    	
				    	if( !faceToken.contains("/") )
				    	{
				    		try {
				    			face[1] = (short) (Short.parseShort(faceToken)-1);
				    		} catch (NumberFormatException e)
				    		{
				    			Log.e("BMB", "Number format exception: " + faceToken);
				    		}
				    		indices.add(face[1]);
				    	}
				    	else
				    	{
				    		StringTokenizer faceTokenizer = new StringTokenizer(faceToken, "/");
				    		face[1] = (short) (Short.parseShort(faceTokenizer.nextToken())-1);
				    		indices.add(face[1]);
				    		//TODO implement face texture and face normal params
				    	}	
				    	
				    	//Some object files will define faces with 4 vertices if so make a new triangle with vertices from the previous face
				    	if( tokenizer.hasMoreTokens() )
				    	{
				    		indices.add(face[0]);
				    		indices.add(face[1]);
				    		faceToken = tokenizer.nextToken();				    	
				    		if( !faceToken.contains("/") )
				    		{
				    			indices.add((short) (Short.parseShort(faceToken)-1));
				    		}
				    		else
				    		{
				    			StringTokenizer faceTokenizer = new StringTokenizer(faceToken, "/");
				    			indices.add((short) (Short.parseShort(faceTokenizer.nextToken())-1));
				    			//TODO implement face texture and face normal params
				    		}
				    	}
				    }
				    else if(rowType.equalsIgnoreCase("vt"))
				    {
				    	//TODO: read in vertex texture coordinates
				    }
				    
			    }
			} 
			catch (IOException e)
			{
				Log.e(Utils.getContext().getString(R.string.log_tag), "Error reading object file.");
			}
		}

		if(!vertices.isEmpty())
		{
			objectFileObject = new ObjectFileGeometry(vertices, normals, indices);
			objectFileObject.setMinMaxX(minX, maxX);
			objectFileObject.setMinMaxY(minY, maxY);
			objectFileObject.setMinMaxZ(minZ, maxZ);
		}
		return objectFileObject;
	}
}