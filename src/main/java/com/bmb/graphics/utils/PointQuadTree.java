package com.bmb.graphics.utils;

import com.bmb.graphics.geometry.Polygon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A quad tree which tracks items with a Point geometry.
 * See http://en.wikipedia.org/wiki/Quadtree for details on the data structure.
 * This class is not thread safe.
 * <p/>
 * Adapted from Google Maps Util Quadtree https://github.com/googlemaps/android-maps-utils
 * Used under Apache 2.0 license http://www.apache.org/licenses/LICENSE-2.0
 */
public class PointQuadTree<T extends PointQuadTree.Item>
{

    public interface Item
    {
        public float getX();

        public float getY();
    }

    /**
     * The bounds of this quad.
     */
    private final Polygon bounds;

    /**
     * The depth of this quad in the tree.
     */
    private final int depth;

    /**
     * Maximum number of elements to store in a quad before splitting.
     */
    private final static int MAX_ELEMENTS = 50;

    /**
     * The elements inside this quad, if any.
     */
    private List<T> items;

    /**
     * Maximum depth.
     */
    private final static int MAX_DEPTH = 40;

    /**
     * Child quads.
     */
    private List<PointQuadTree<T>> children = null;

    /**
     * Creates a new quad tree with specified bounds.
     *
     * @param minX The minimum x value for the tree
     * @param maxX The maximum x value for the tree
     * @param minY The minimum y value for the tree
     * @param maxY The maximum y value for the tree
     */
    public PointQuadTree(float minX, float maxX, float minY, float maxY)
    {
        this(new Polygon(minX, maxX, minY, maxY));
    }

    public PointQuadTree(Polygon bounds)
    {
        this(bounds, 0);
    }

    private PointQuadTree(float minX, float maxX, float minY, float maxY, int depth)
    {
        this(new Polygon(minX, maxX, minY, maxY), depth);
    }

    private PointQuadTree(Polygon bounds, int depth)
    {
        this.bounds = bounds;
        this.depth = depth;
    }

    /**
     * Insert an item.
     */
    public void add(T item)
    {
        float x = item.getX();
        float y = item.getY();
        if( this.bounds.simpleContains(x, y) )
        {
            insert(x, y, item);
        }
    }

    public void add(List<T> items)
    {
        for( T item : items )
        {
            float x = item.getX();
            float y = item.getY();
            if( this.bounds.simpleContains(x, y) )
            {
                insert(x, y, item);
            }
        }
    }

    private void insert(float x, float y, T item)
    {
        if( this.children != null )
        {
            if( y < bounds.getCenter().y )
            {
                if( x < bounds.getCenter().x )
                { // top left
                    children.get(0).insert(x, y, item);
                }
                else
                { // top right
                    children.get(1).insert(x, y, item);
                }
            }
            else
            {
                if( x < bounds.getCenter().x )
                { // bottom left
                    children.get(2).insert(x, y, item);
                }
                else
                {
                    children.get(3).insert(x, y, item);
                }
            }
            return;
        }
        if( items == null )
        {
            items = new ArrayList<>();
        }
        items.add(item);
        if( items.size() > MAX_ELEMENTS && depth < MAX_DEPTH )
        {
            split();
        }
    }

    /**
     * Split this quad.
     */
    private void split()
    {
        children = new ArrayList<>(4);
        children.add(new PointQuadTree<T>(bounds.getMinX(), bounds.getCenter().x, bounds.getMinY(), bounds.getCenter().y, depth + 1));
        children.add(new PointQuadTree<T>(bounds.getCenter().x, bounds.getMaxX(), bounds.getMinY(), bounds.getCenter().y, depth + 1));
        children.add(new PointQuadTree<T>(bounds.getMinX(), bounds.getCenter().x, bounds.getCenter().y, bounds.getMaxY(), depth + 1));
        children.add(new PointQuadTree<T>(bounds.getCenter().x, bounds.getMaxX(), bounds.getCenter().y, bounds.getMaxY(), depth + 1));

        for( T item : items )
        {
            // re-insert items into child quads.
            insert(item.getX(), item.getY(), item);
        }

        this.items = null;
    }

    /**
     * Remove the given item from the set.
     *
     * @return whether the item was removed.
     */
    public boolean remove(T item)
    {
        float x = item.getX();
        float y = item.getY();
        return this.bounds.simpleContains(x, y) && remove(x, y, item);
    }

    private boolean remove(float x, float y, T item)
    {
        if( this.children != null )
        {
            if( y < bounds.getCenter().y )
            {
                if( x < bounds.getCenter().x )
                { // top left
                    return children.get(0).remove(x, y, item);
                }
                else
                { // top right
                    return children.get(1).remove(x, y, item);
                }
            }
            else
            {
                if( x < bounds.getCenter().x )
                { // bottom left
                    return children.get(2).remove(x, y, item);
                }
                else
                {
                    return children.get(3).remove(x, y, item);
                }
            }
        }
        else
        {
            return items != null && items.remove(item);
        }
    }

    public void updateItem(T item, float oldX, float oldY)
    {
        remove(oldX, oldY, item);
        add(item);
    }

    private PointQuadTree findNodeForPosition(float x, float y)
    {
        PointQuadTree containingTree = null;
        if( bounds.simpleContains(x, y) )
        {
            if( children != null )
            {
                if( y < bounds.getCenter().y )
                {
                    if( x < bounds.getCenter().x )
                    { // top left
                        containingTree = children.get(0).findNodeForPosition(x, y);
                    }
                    else
                    {
                        containingTree = children.get(1).findNodeForPosition(x, y);
                    }
                }
                else
                {
                    if( x < bounds.getCenter().x )
                    { // bottom left
                        containingTree = children.get(2).findNodeForPosition(x, y);
                    }
                    else
                    {
                        containingTree = children.get(3).findNodeForPosition(x, y);
                    }
                }
            }
            else
            {
                containingTree = this;
            }
        }

        return containingTree;
    }
    /**
     * Removes all points from the quadTree
     */
    public void clear()
    {
        children = null;
        if( items != null )
        {
            items.clear();
        }
    }

    /**
     * Search for all items within a given bounds.
     */
    public Collection<T> search(Polygon searchBounds)
    {
        final List<T> results = new ArrayList<>();
        search(searchBounds, results);
        return results;
    }

    private void search(Polygon searchBounds, Collection<T> results)
    {
        if( !bounds.intersects(searchBounds) )
        {
            return;
        }

        if( this.children != null )
        {
            for( PointQuadTree<T> quad : children )
            {
                quad.search(searchBounds, results);
            }
        }
        else if( items != null )
        {
            if( searchBounds.contains(bounds) )
            {
                results.addAll(items);
            }
            else
            {
                for( T item : items )
                {
                    if( searchBounds.simpleContains(item.getX(), item.getY()) )
                    {
                        results.add(item);
                    }
                }
            }
        }
    }
}
