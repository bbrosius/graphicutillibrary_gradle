package com.bmb.graphics.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import com.bmb.graphic.utils.R;
import com.bmb.graphics.geometry.Vertex;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

public class GraphicUtils
{
    public static final int FLOAT_SIZE_BYTES = 4;
    public static final int DATA_STRIDE = 3 * FLOAT_SIZE_BYTES;
    private static final float[] rotationMatrix = new float[16];
    private static final float[] fullRotationMatrix = new float[32];
    private static final float[] src = new float[16];
    private static final float[] tmp = new float[12];

	public static FloatBuffer convertFloatArrayToFloatBuffer(float [] array)
	{
		ByteBuffer tempBuffer = ByteBuffer.allocateDirect(array.length * 4);
        tempBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer buffer = tempBuffer.asFloatBuffer();
        buffer.put(array);
        buffer.position(0);
        return buffer;
	}
	
	public static float[] convertFloatArrayListToFloatArray(ArrayList<Float> floatList)
    {
    	float[] array = new float[floatList.size()];
    	for(int i = 0; i < floatList.size(); i++)
    	{
    		array[i] = floatList.get(i);
    	}
    	
    	return array;
    }
	 
	public static float[] convertVertexArrayListToFloatArray(ArrayList<Vertex> vertexList)
	{
		float[] array = new float[vertexList.size()*3];
		for(int i = 0; i < vertexList.size(); i++)
		{
			array[i * 3] = vertexList.get(i).x;
			array[i * 3 +1] = vertexList.get(i).y;
			array[i * 3 +2] = vertexList.get(i).z;    	
		}
		return array;
	}
	
	public static float[] convertVertexArrayToFloatArray(Vertex[] vertices)
	{
		float[] array = new float[vertices.length*3];
		for( int i = 0; i < vertices.length; i++)
		{
			array[i * 3] = vertices[i].x;
			array[i * 3 + 1] = vertices[i].y;
			array[i * 3 + 2] = vertices[i].z;
		}
		return array;
	}
	
	public static FloatBuffer convertFloatArrayListToFloatBuffer(ArrayList<Float> floatList)
	{
		float[] array = convertFloatArrayListToFloatArray(floatList);
		return convertFloatArrayToFloatBuffer(array);
	}
	
	public static FloatBuffer convertVertexArrayListToFloatBuffer(ArrayList<Vertex> vertexList)
	{
		float[] array = convertVertexArrayListToFloatArray(vertexList);
		return convertFloatArrayToFloatBuffer(array);		
	}
	
	public static float getScreenDensity(Context context)
	{
		WindowManager service = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		service.getDefaultDisplay().getMetrics(metrics);
		
		return metrics.density;
	}

	public static void checkGlError(String op)
	{
		int error;
		if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR)
		{
			if(com.bmb.utils.Utils.getContext() != null)
			{
				Log.e(com.bmb.utils.Utils.getContext().getString(R.string.log_tag), op + ": glError " + error);
			}
		}
	}
	
	public static ByteBuffer convertByteArrayToByteBuffer(byte[] bytes)
	{
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bytes.length);
		byteBuffer.put(bytes);
		byteBuffer.position(0);
		
		return byteBuffer;
	}
	
	public static ByteBuffer convertByteArrayListToByteBuffer(ArrayList<Byte> bytes)
	{
		byte[] byteArray = convertByteArrayListToByteArray(bytes);
		return convertByteArrayToByteBuffer(byteArray);
	}
	
	public static byte[] convertByteArrayListToByteArray(ArrayList<Byte> bytes)
	{
		byte[] byteArray = new byte[bytes.size()];
		for( int i = 0; i < bytes.size(); i++)
		{
			byteArray[i] = bytes.get(i);
		}
		
		return byteArray;
	}
	
	public static ShortBuffer convertShortArrayToShortBuffer(short [] array)
	{
		ByteBuffer tempBuffer = ByteBuffer.allocateDirect(array.length * 2);
        tempBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer buffer = tempBuffer.asShortBuffer();
        buffer.put(array);
        buffer.position(0);
        return buffer;
	}
	
	public static ShortBuffer convertShortArrayListToShortBuffer(ArrayList<Short> shorts)
	{
		short[] shortArray = convertShortArrayListToShortArray(shorts);
		return convertShortArrayToShortBuffer(shortArray);
	}
	
	public static short[] convertShortArrayListToShortArray(ArrayList<Short> shorts)
	{
		short[] shortArray = new short[shorts.size()];
		for( int i = 0; i < shorts.size(); i++)
		{
			shortArray[i] = shorts.get(i);
		}
		
		return shortArray;
	}
	
	public static void rotateAboutAxis(float[] result, int resultOffset, float angle, float x, float y, float z)
	{
		Matrix.setRotateM(fullRotationMatrix, 0, angle, x, y, z);
		Matrix.multiplyMM(fullRotationMatrix, 16, result, resultOffset, fullRotationMatrix, 0);
		System.arraycopy(fullRotationMatrix, 16, result, resultOffset, 16);
	}
	
	public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) 
	{
		// Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio  = Math.round((float) width / (float) reqWidth);

	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }

	    return inSampleSize;
	}

    /**
     * Loads a bitmap and sets it on the ImageView
     * @param imageUri
     * @param desiredWidth
     * @param desiredHeight
     * @param context
     * @param imageView
     */
    public static void loadBitmap(Uri imageUri, int desiredWidth, int desiredHeight, Context context, ImageView imageView)
    {
        if (BitmapLoaderTask.cancelPotentialWork(imageUri, imageView)) {
            final Bitmap bitmap = getBitmapFromMemCache(imageUri);
            if (bitmap != null)
            {
                imageView.setImageBitmap(bitmap);
            }
            else
            {
                final BitmapLoaderTask task = new BitmapLoaderTask(imageView, desiredWidth, desiredHeight);
                final BitmapLoaderTask.AsyncDrawable asyncDrawable =
                        new BitmapLoaderTask.AsyncDrawable(context.getResources(), getLoadingBitmap(context), task);
                imageView.setImageDrawable(asyncDrawable);
                task.execute(imageUri);
            }

        }
    }

    public static Bitmap loadBitmap(Uri imageUri, int desiredWidth, int desiredHeight, Context context) throws FileNotFoundException
    {
        // First we get the the dimensions of the file on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri), null, options);

        int sampleSize = calculateInSampleSize(options, desiredWidth, desiredHeight);

        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri), null, options);
    }

    private static Bitmap getLoadingBitmap(Context context)
    {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.empty_photo);
    }

    public static void addBitmapToMemoryCache(Uri key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            ImageCache.getInstance().put(key, bitmap);
        }
    }

    public static Bitmap getBitmapFromMemCache(Uri key) {
        return ImageCache.getInstance().get(key);
    }

    /**
     * Converts a bitmap to a string which can be serialized to a file.
     * @param bitmapPicture
     * @return
     */
    public static String getStringFromBitmap(Bitmap bitmapPicture)
    {

        final int COMPRESSION_QUALITY = 100;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * Converts a string back into a bitmap.
     * @param imageString the string representing the bytes of the image
     * @return
     */
    public static Bitmap getBitmapFromString(String imageString)
    {
        byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

	/*public static void setRotateM(float[] rm, int rmOffset,
	            float a, float x, float y, float z) 
	{
		rm[rmOffset + 3] = 0;
		rm[rmOffset + 7] = 0;
		rm[rmOffset + 11]= 0;
		rm[rmOffset + 12]= 0;
		rm[rmOffset + 13]= 0;
		rm[rmOffset + 14]= 0;
		rm[rmOffset + 15]= 1;
		
		a *= (float) (Math.PI / 180.0f);
		
		float s = (float) Math.sin(a);
		float c = (float) Math.cos(a);
		
		if (1.0f == x && 0.0f == y && 0.0f == z) 
		{
			rm[rmOffset + 5] = c;   rm[rmOffset + 10]= c;
			rm[rmOffset + 6] = s;   rm[rmOffset + 9] = -s;
			rm[rmOffset + 1] = 0;   rm[rmOffset + 2] = 0;
			rm[rmOffset + 4] = 0;   rm[rmOffset + 8] = 0;
			rm[rmOffset + 0] = 1;
		} 
		else if (0.0f == x && 1.0f == y && 0.0f == z) 
		{
			rm[rmOffset + 0] = c;   rm[rmOffset + 10]= c;
			rm[rmOffset + 8] = s;   rm[rmOffset + 2] = -s;
			rm[rmOffset + 1] = 0;   rm[rmOffset + 4] = 0;
			rm[rmOffset + 6] = 0;   rm[rmOffset + 9] = 0;
			rm[rmOffset + 5] = 1;
		} 
		else if (0.0f == x && 0.0f == y && 1.0f == z) 
		{
			rm[rmOffset + 0] = c;   rm[rmOffset + 5] = c;
			rm[rmOffset + 1] = s;   rm[rmOffset + 4] = -s;
			rm[rmOffset + 2] = 0;   rm[rmOffset + 6] = 0;
			rm[rmOffset + 8] = 0;   rm[rmOffset + 9] = 0;
			rm[rmOffset + 10]= 1;
		} 
		else 
		{
			float len = Matrix.length(x, y, z);
			if (1.0f != len) 
			{
				float recipLen = 1.0f / len;
				x *= recipLen;
				y *= recipLen;
				z *= recipLen;
			}
			
			float nc = 1.0f - c;
			float xy = x * y;
			float yz = y * z;
			float zx = z * x;
			float xs = x * s;
			float ys = y * s;
			float zs = z * s;
			rm[rmOffset +  0] = x*x*nc +  c;
			rm[rmOffset +  4] =  xy*nc - zs;
			rm[rmOffset +  8] =  zx*nc + ys;
			rm[rmOffset +  1] =  xy*nc + zs;
			rm[rmOffset +  5] = y*y*nc +  c;
			rm[rmOffset +  9] =  yz*nc - xs;
			rm[rmOffset +  2] =  zx*nc - ys;
			rm[rmOffset +  6] =  yz*nc + xs;
			rm[rmOffset + 10] = z*z*nc +  c;
		}
	}*/
}