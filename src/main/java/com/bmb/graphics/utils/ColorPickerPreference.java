package com.bmb.graphics.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bmb.graphic.utils.R;
import com.bmb.graphics.utils.ColorPicker.OnColorChangedListener;

public class ColorPickerPreference extends Preference implements OnColorChangedListener
{
	private Context context;
	private static final int IMAGE_SIZE = 50;
	private ShapeDrawable swatchDrawable;
    private Paint prefPaint;

	private int color;
	private ImageView swatch;
	
	public ColorPickerPreference(Context context)
	{
		super(context);
		this.context = context;
	}
	
	public ColorPickerPreference(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.context = context;
	}
	
	public ColorPickerPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
	}
	
	@Override
	protected View onCreateView(ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater)context.getSystemService
	      (Context.LAYOUT_INFLATER_SERVICE);
		
		LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.color_preference_layout, null);
		TextView title = (TextView) layout.findViewById(R.id.color_preference_title);
		title.setText(getTitle());
		
		prefPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        prefPaint.setStrokeWidth(5);
        
		swatch = (ImageView) layout.findViewById(R.id.color_swatch);
		setImageColor();
		
    	swatchDrawable = new ShapeDrawable(new RectShape());
		swatchDrawable.setIntrinsicHeight(IMAGE_SIZE);
		swatchDrawable.setIntrinsicWidth(IMAGE_SIZE);		
		swatchDrawable.getPaint().set(prefPaint);		
		swatch.setImageDrawable(swatchDrawable);
		
		layout.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				ColorPicker picker = new ColorPicker(context, ColorPickerPreference.this, color);
				picker.show();
			}
			
		});
	   		
	    return layout;	
	}
	
	@Override 
	protected Object onGetDefaultValue(TypedArray ta,int index)
	{
		int dValue = (int)ta.getInt(index,50);
		return dValue;
	}
	 
	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
	{
		int temp = restoreValue ? getPersistedInt(Color.WHITE) : (Integer)defaultValue;
	 
		if(!restoreValue)
			persistInt(temp);
		color = temp;
	}
 
	private void updatePreference(int newValue)
	{
		SharedPreferences.Editor editor =  getEditor();
		editor.putInt(getKey(), newValue);
		editor.commit();
	}
	
	private void setImageColor()
	{
        prefPaint.setColor(color);
	}

	@Override
	public void colorChanged(int color)
	{
		if(!callChangeListener(color))
		{
			return; 
		}
		
		this.color = color;
		setImageColor();
		updateSwatch();
		updatePreference(color);		
	}

    public void setColor(int color)
    {
        this.color = color;
    }
	
	private void updateSwatch()
	{
		if( swatch != null && swatchDrawable != null )
		{
			swatchDrawable.getPaint().set(prefPaint);
			swatch.postInvalidate();
		}	
	}
	
}