package com.bmb.graphics.utils;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.util.LruCache;

/**
 * Created by Ben on 4/16/2014.
 */
public class ImageCache extends LruCache<Uri, Bitmap>
{
    private static ImageCache imageCache;

    private ImageCache(int cacheSize)
    {
        super(cacheSize);
    }

    public static ImageCache getInstance()
    {
        if( imageCache == null )
        {
            // Get max available VM memory, exceeding this amount will throw an
            // OutOfMemory exception. Stored in kilobytes as LruCache takes an
            // int in its constructor.
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 12;

            imageCache = new ImageCache(cacheSize);
        }

        return imageCache;
    }

    @Override
    protected int sizeOf(Uri key, Bitmap bitmap) {
        // The cache size will be measured in kilobytes rather than
        // number of items.
        return (int) getSizeInBytes(bitmap) / 1024;
    }

    public static long getSizeInBytes(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }
}
