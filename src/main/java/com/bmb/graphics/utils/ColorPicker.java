package com.bmb.graphics.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bmb.graphic.utils.R;
import com.bmb.utils.Utils;

public class ColorPicker extends Dialog
{
    public interface OnColorChangedListener
    {
        void colorChanged(int color);
    }
    
    private static final float PI = 3.1415926f;

    private OnColorChangedListener colorChangedListener;
    private int initialColor;
    private Paint colorWheelPaint;
    private Paint selectedColorPaint;
    
    private int[] mColors;
    private Shader colorShader;
    private Paint shaderPaint;
    private int[] detailedColors;
        
	private ImageView colorSelectionSquare;
	private ImageView colorGradientCircle;
	private ImageView colorGradientRectangle;
	private RelativeLayout colorPickerView;
	private ShapeDrawable selectionSquareDrawable;
	private ShapeDrawable detailedGradientDrawable;
	
	private int colorSelectionBoxSize = 60;
	private int quickColorBoxSize = 40;
    private int circleMargin = 24;
    private int selectorOffset = 7;
    private int detailedGradientHeight = 180;
    private int detailedGradientWidth = 40;
    private int gradientStrokeWidth = 32;
    private int gradientRadius = 160;
	
    public ColorPicker(Context context, OnColorChangedListener listener, int initialColor)
    {
        super(context);
        
        colorChangedListener = listener;
        this.initialColor = initialColor;
    }
    
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
     
        mColors = new int[]
        {
            0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00,
            0xFFFFFF00, 0xFFFF0000
        };
            
        //Set up all the dimensions for different screen res
        colorSelectionBoxSize = (int) (colorSelectionBoxSize * GraphicUtils.getScreenDensity(getContext()));
        quickColorBoxSize = (int) (quickColorBoxSize * GraphicUtils.getScreenDensity(getContext()));
        circleMargin = (int) (circleMargin * GraphicUtils.getScreenDensity(getContext()));
        selectorOffset = (int) (selectorOffset * GraphicUtils.getScreenDensity(getContext()));
        detailedGradientHeight = (int) (detailedGradientHeight * GraphicUtils.getScreenDensity(getContext()));
        detailedGradientWidth = (int) (detailedGradientWidth * GraphicUtils.getScreenDensity(getContext()));
        gradientStrokeWidth = (int)(gradientStrokeWidth * GraphicUtils.getScreenDensity(getContext()));
        gradientRadius = (int)(gradientRadius * GraphicUtils.getScreenDensity(getContext()));
        
        Shader s = new SweepGradient(100 * GraphicUtils.getScreenDensity(getContext()), 100 * GraphicUtils.getScreenDensity(getContext()), mColors, null);

        colorWheelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        colorWheelPaint.setShader(s);
        colorWheelPaint.setStyle(Paint.Style.STROKE);
        colorWheelPaint.setStrokeWidth(gradientStrokeWidth);
        
        selectedColorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        selectedColorPaint.setColor(initialColor);
        selectedColorPaint.setStrokeWidth(5);
          
        detailedColors = new int[3];
        detailedColors[0] = Color.BLACK;
        detailedColors[1] = selectedColorPaint.getColor();
        detailedColors[2] = Color.WHITE;
        
        colorShader = new LinearGradient(0, 2, detailedGradientWidth, detailedGradientHeight, detailedColors, null, Shader.TileMode.CLAMP );
        shaderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shaderPaint.setStyle(Paint.Style.FILL);
        shaderPaint.setShader(colorShader);
        
        LayoutInflater inflater = (LayoutInflater)   getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
        colorPickerView = (RelativeLayout) inflater.inflate(R.layout.color_picker_layout, null);
        
        createView();
        setContentView(colorPickerView);
        //getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        setTitle(getContext().getString(R.string.color_preview));
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event)
	{ 
    	if (keyCode == KeyEvent.KEYCODE_BACK)
    	{ 
    		colorChangedListener.colorChanged(selectedColorPaint.getColor());
    		dismiss();    		
    	}
    	return true;
	}
    
    private void createView()
	{
    	colorSelectionSquare = (ImageView) colorPickerView.findViewById(R.id.color_selection_box);
    	selectionSquareDrawable = new ShapeDrawable();
		selectionSquareDrawable.setIntrinsicHeight(colorSelectionBoxSize);
		selectionSquareDrawable.setIntrinsicWidth(colorSelectionBoxSize);		
		selectionSquareDrawable.getPaint().set(selectedColorPaint);		
		colorSelectionSquare.setImageDrawable(selectionSquareDrawable);
		
		colorSelectionSquare.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				colorChangedListener.colorChanged(selectedColorPaint.getColor());
				dismiss();
			}			
		});
		
		colorGradientCircle = (ImageView) colorPickerView.findViewById(R.id.color_selection_wheel);
		int bitmapSize = (int)( 200 * GraphicUtils.getScreenDensity(getContext()));
		Bitmap gradientBitmap = Bitmap.createBitmap(bitmapSize, bitmapSize, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(gradientBitmap);
	    canvas.drawOval(new RectF(gradientStrokeWidth, gradientStrokeWidth, gradientRadius, gradientRadius), colorWheelPaint);            
		colorGradientCircle.setImageBitmap(gradientBitmap);
				
		colorGradientRectangle = (ImageView) colorPickerView.findViewById(R.id.detailed_color_selection_box);	
		detailedGradientDrawable = new ShapeDrawable();
		detailedGradientDrawable.setIntrinsicHeight(detailedGradientHeight);
		detailedGradientDrawable.setIntrinsicWidth(detailedGradientWidth);
		detailedGradientDrawable.getPaint().set(shaderPaint);
		colorGradientRectangle.setImageDrawable(detailedGradientDrawable);
		
		ImageView quickColorOne = (ImageView) colorPickerView.findViewById(R.id.quick_color_one);
		ShapeDrawable quickColorOneDrawable = new ShapeDrawable();
		quickColorOneDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorOneDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorOnePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorOnePaint.setColor(Color.BLUE);
        quickColorOnePaint.setStrokeWidth(5);
		quickColorOneDrawable.getPaint().set(quickColorOnePaint);		
		quickColorOne.setImageDrawable(quickColorOneDrawable);
		quickColorOne.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.BLUE);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		
		ImageView quickColorTwo = (ImageView) colorPickerView.findViewById(R.id.quick_color_two);
		ShapeDrawable quickColorTwoDrawable = new ShapeDrawable();
		quickColorTwoDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorTwoDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorTwoPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorTwoPaint.setColor(Color.CYAN);
        quickColorTwoPaint.setStrokeWidth(5);
		quickColorTwoDrawable.getPaint().set(quickColorTwoPaint);		
		quickColorTwo.setImageDrawable(quickColorTwoDrawable);
		quickColorTwo.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.CYAN);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorThree = (ImageView) colorPickerView.findViewById(R.id.quick_color_three);
		ShapeDrawable quickColorThreeDrawable = new ShapeDrawable();
		quickColorThreeDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorThreeDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorThreePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorThreePaint.setColor(Color.GREEN);
        quickColorThreePaint.setStrokeWidth(5);
		quickColorThreeDrawable.getPaint().set(quickColorThreePaint);		
		quickColorThree.setImageDrawable(quickColorThreeDrawable);
		quickColorThree.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.GREEN);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorFour = (ImageView) colorPickerView.findViewById(R.id.quick_color_four);
		ShapeDrawable quickColorFourDrawable = new ShapeDrawable();
		quickColorFourDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorFourDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorFourPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorFourPaint.setColor(Color.MAGENTA);
        quickColorFourPaint.setStrokeWidth(5);
		quickColorFourDrawable.getPaint().set(quickColorFourPaint);		
		quickColorFour.setImageDrawable(quickColorFourDrawable);
		quickColorFour.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.MAGENTA);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorFive = (ImageView) colorPickerView.findViewById(R.id.quick_color_five);
		ShapeDrawable quickColorFiveDrawable = new ShapeDrawable();
		quickColorFiveDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorFiveDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorFivePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorFivePaint.setColor(Color.RED);
        quickColorFivePaint.setStrokeWidth(5);
		quickColorFiveDrawable.getPaint().set(quickColorFivePaint);		
		quickColorFive.setImageDrawable(quickColorFiveDrawable);
		quickColorFive.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.RED);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorSix = (ImageView) colorPickerView.findViewById(R.id.quick_color_six);
		ShapeDrawable quickColorSixDrawable = new ShapeDrawable();
		quickColorSixDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorSixDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorSixPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorSixPaint.setColor(Color.YELLOW);
        quickColorSixPaint.setStrokeWidth(5);
		quickColorSixDrawable.getPaint().set(quickColorSixPaint);		
		quickColorSix.setImageDrawable(quickColorSixDrawable);
		quickColorSix.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.YELLOW);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorSeven = (ImageView) colorPickerView.findViewById(R.id.quick_color_seven);
		ShapeDrawable quickColorSevenDrawable = new ShapeDrawable();
		quickColorSevenDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorSevenDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorSevenPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorSevenPaint.setColor(Color.BLACK);
        quickColorSevenPaint.setStrokeWidth(5);
		quickColorSevenDrawable.getPaint().set(quickColorSevenPaint);		
		quickColorSeven.setImageDrawable(quickColorSevenDrawable);
		quickColorSeven.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.BLACK);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		ImageView quickColorEight = (ImageView) colorPickerView.findViewById(R.id.quick_color_eight);
		ShapeDrawable quickColorEightDrawable = new ShapeDrawable();
		quickColorEightDrawable.setIntrinsicHeight(quickColorBoxSize);
		quickColorEightDrawable.setIntrinsicWidth(quickColorBoxSize);	
		Paint quickColorEightPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        quickColorEightPaint.setColor(Color.WHITE);
        quickColorEightPaint.setStrokeWidth(5);
		quickColorEightDrawable.getPaint().set(quickColorEightPaint);		
		quickColorEight.setImageDrawable(quickColorEightDrawable);
		quickColorEight.setOnClickListener(new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				selectedColorPaint.setColor(Color.WHITE);
				if( detailedColors != null && detailedColors.length > 2)
        		{
        			detailedColors[1] = selectedColorPaint.getColor();
        		}
				updateView();
			}			
		});
		
		EditText hexCodeEntry = (EditText) colorPickerView.findViewById(R.id.hex_text_entry);
		hexCodeEntry.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable hex) 
			{
				if(hex.length() > 0)
				{
					if(hex.charAt(0) == '#')
					{
						if(hex.length() == 7)
						{
							String colorString = hex.toString();
							if( Utils.isHex(colorString))
							{
							    selectedColorPaint.setColor(Color.parseColor(colorString));
								if( detailedColors != null && detailedColors.length > 2)
				        		{
				        			detailedColors[1] = selectedColorPaint.getColor();
				        		}
								updateView();
							}
						}
					}
					else
					{
						if(hex.length() == 6)
						{
							String colorString = "#" + hex.toString();
							if( Utils.isHex(colorString) )
							{
								selectedColorPaint.setColor(Color.parseColor(colorString));
								if( detailedColors != null && detailedColors.length > 2)
				        		{
				        			detailedColors[1] = selectedColorPaint.getColor();
				        		}
								updateView();
							}
						}
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) 
			{
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) 
			{
				
			}
			
		});

	}
    
    private void updateView()
    {
		if( colorSelectionSquare != null && selectionSquareDrawable != null )
		{
			selectionSquareDrawable.getPaint().set(selectedColorPaint);
			colorSelectionSquare.postInvalidate();
		}		
					
		if( detailedGradientDrawable != null && colorGradientRectangle != null)
		{
			colorShader = new LinearGradient(0, 0, detailedGradientWidth, detailedGradientHeight, detailedColors, null, Shader.TileMode.CLAMP );
			shaderPaint.setShader(colorShader);
			
			detailedGradientDrawable.getPaint().set(shaderPaint);
			colorGradientRectangle.postInvalidate();
		}
    }
    
    private int ave(int s, int d, float p)
    {
        return s + java.lang.Math.round(p * (d - s));
    }
    
    private int interpColor(int colors[], float unit)
    {
        if (unit <= 0) {
            return colors[0];
        }
        if (unit >= 1) {
            return colors[colors.length - 1];
        }
        
        float p = unit * (colors.length - 1);
        int i = (int)p;
        p -= i;

        // now p is just the fractional part [0...1) and i is the index
        int c0 = colors[i];
        int c1 = colors[i+1];
        int a = ave(Color.alpha(c0), Color.alpha(c1), p);
        int r = ave(Color.red(c0), Color.red(c1), p);
        int g = ave(Color.green(c0), Color.green(c1), p);
        int b = ave(Color.blue(c0), Color.blue(c1), p);
        
        return Color.argb(a, r, g, b);
    }        

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
    	Rect gradientCircleRect = new Rect();
    	colorGradientCircle.getGlobalVisibleRect(gradientCircleRect);
    	Rect gradientRect = new Rect();
    	colorGradientRectangle.getGlobalVisibleRect(gradientRect);
    	       
        
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                if( gradientCircleRect.contains((int)event.getX(), (int)event.getY() ) )
            	{
            		float centerX = gradientCircleRect.exactCenterX();
            		float centerY = gradientCircleRect.exactCenterY();
            		float angle = (float)java.lang.Math.atan2(event.getY() - centerY, event.getX() - centerX);
                    // need to turn angle [-PI ... PI] into unit [0....1]
                    float unit = angle/(2*PI);
                    if (unit < 0)
                    {
                        unit += 1;
                    }
                    
                    selectedColorPaint.setColor(interpColor(mColors, unit));
                    if( detailedColors != null && detailedColors.length > 2)
            		{
            			detailedColors[1] = selectedColorPaint.getColor();
            		}
                    
                    updateView();
            	}
            	else if( gradientRect.contains((int) event.getX(), (int)event.getY()))
            	{
            		float unit = 0.0f;
            		unit = ( event.getY()- gradientRect.top) / detailedGradientHeight;
                    selectedColorPaint.setColor(interpColor(detailedColors, unit));
                    updateView();
            	}
                break;
            case MotionEvent.ACTION_UP:
                updateView();
                break;
        }
        return false;
    }  
}