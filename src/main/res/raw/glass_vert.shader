uniform mat4 modelViewProjectionMatrix;

uniform mat4 modelViewMatrix;
attribute vec3 position;


//The direction vector for the light source
varying vec3 pos;

void main()
{
	pos = vec3(modelViewMatrix * vec4(position, 1.0));
	
	gl_Position = modelViewProjectionMatrix * vec4(position, 1.0);
}