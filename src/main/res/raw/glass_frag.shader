precision mediump float;

uniform vec3 lightPosition;
uniform vec3 eyePos;
varying vec3 pos;

void main()
{	
	//This is the ambient light color
	vec4 color = vec4(0,0,0,0);
	
	vec3 normalN = normalize(pos);
	vec3 lightDir = normalize(lightPosition - pos);
	
	float diffuse = max(dot(normalN, lightDir), 0.1);
	//diffuse = diffuse * (1.0 / (1.0 + (0.10 * distance)));
	
	vec3 diffuseColor = vec3(1, 1, 1);
	
	float specular = 0.0;
	vec3 specularColor = vec3(1, 1, 1);
	
	//Removed check for diffuse being 0, this could cause highlight where there is no diffuse light but shouldn't
	vec3 eyeVec = normalize(pos-eyePos);
		
	vec3 reflectVector = reflect(-lightDir, normalN);
	specular = pow(max(dot(reflectVector, eyeVec), 0.0), 90.0);

	
	vec4 finalColor =  vec4((diffuse * diffuseColor), .05)  + vec4((specular * specularColor), .3) + color;
	finalColor.a = .1 + specular * .5;
	gl_FragColor = finalColor;
	
	//gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);
}