precision mediump float;

uniform mat4 modelViewProjectionMatrix;
attribute vec3 position;
varying vec3 pos;

void main()
{
	gl_Position = modelViewProjectionMatrix * vec4(position, 1.0);
	pos = gl_Position.xyz;
}
 